
//#define DEBUG
#define VERBOSE_DEBUG
#define MODULE_NAME "dct-dphy-rx"

#include "dct-dphy-rx.h"

#include "zuk_mipi_rx_rb_regs.h"
#include "dct_reg_helper.h"
#include "isp_pre_functions.h"

// tests codes
#define RDWR_RX_SYS_0 0x01
#define FORCE_STATE_MASK 0x01
#define HSFREQRANGE_OVR_EN_MASK 0x20

#define RDWR_RX_SYS_1 0x02
#define TIMEBASE_OVR_EN_MASK 0x80
#define HSFREQRANGE_OVR_MASK 0x7f

#define RDWR_RX_SYS_2 0x03
#define RDWR_RX_SYS_3 0x04
#define RDWR_RX_SYS_4 0x05
#define RDWR_RX_SYS_5 0x06
#define RDWR_RX_SYS_6 0x07
#define RDWR_RX_SYS_7 0x08
#define RDWR_RX_SYS_8 0x09
#define RDWR_RX_SYS_9 0x0a

#define RDWR_RX_RX_STARTUP_OVR_0 0xe0
#define CLK_EN_LANES_BYPASS_MASK 0x01
#define CLK_EN_LANES_TESTER_MASK 0x02
#define BYPASS_OFFSET_MACHINE_MASK 0x04
#define BYPASS_DDLTUNNING_MACHINE_MASK 0x08
#define SKEW_BACK_COMP_EN_OVR_EN_MASK 0x10
#define SKEW_BACK_COMP_EN_OVR_MASK 0x20
#define BYPASS_SKEW_MACHINE_MASK 0x40
#define RX_RXHS_COMPATIBILITY_MODE_OVR_EN_MASK 0x80

#define RDWR_RX_RX_STARTUP_OVR_1 0xe1
#define RX_RXHS_COMPATIBILITY_MODE_OVR_MASK 0x01
#define RXCLK_RXHS_DDR_CLK_EN_BYPASS_MASK 0x02
#define ENABLE_LANES_OVR_EN_MASK 0x04
#define ENABLE_LANES_OVR_MASK 0x08
#define DESKEW_LEGACYZ_OVR_MASK 0x10
#define DESKEW_LEGACYZ_OVR_EN_MASK 0x20

#define RDWR_RX_RX_STARTUP_OVR_2 0xe2

#define RDWR_RX_RX_STARTUP_OVR_3 0xe3

#define RDWR_RX_RX_STARTUP_OVR_4 0xe4
#define DDL_OSC_FREQ_TARGET_OVR_EN_MASK 0x01
#define COUNTER_FOR_DES_EN_CONFIG_IF_MASK 0xf0

#define RDWR_RX_RX_STARTUP_OVR_5 0xe5
#define COUNTER_FOR_DES_EN_BYPASS_MASK 0x01

#define RD_RX_RX_STARTUP_OBS_0 0xc7
#define RD_RX_RX_STARTUP_OBS_1 0xc8
#define RD_RX_RX_STARTUP_OBS_2 0xc9
#define RD_RX_RX_STARTUP_OBS_3 0xca
#define RD_RX_RX_STARTUP_OBS_4 0xcb

#define RD_TX_SLEW_0 0x280 //in TX config space
#define RDWR_TX_SLEW_0 0x26b //in TX config space


#define RDWR_RX_CB_1 0x1ab
#define RDWR_RX_CB_2 0x1ac
#define RDWR_RX_CLKLANE_LANE_4 0x305
#define RDWR_RX_CLKLANE_LANE_6 0x307
#define RDWR_RX_LANE0_DDL_1 0x607
#define RDWR_RX_LANE1_DDL_1 0x807
#define RDWR_RX_LANE2_DDL_1 0xa07
#define RDWR_RX_LANE3_DDL_1 0xc07

#define R_CSI2_DPHY_SHUTDOWNZ 	0x0
#define R_CSI2_DPHY_RSTZ 		0x4
#define R_CSI2_DPHY_RX 			0x8
#define	R_CSI2_DPHY_STOPSTATE 	0xC
#define R_CSI2_DPHY_TST_CTRL0 	0x10
#define R_CSI2_DPHY_TST_CTRL1 	0x14

/* N_LANES_REG */
#define N_LANES_REG 			0x04

#define CSI2_PPI_PG_CONFIG 	 	0x68
#define CSI2_PPI_PG_ENABLE  	0x6c
#define CSI2_PPI_PG_STATUS		0x70

/* the phy offset of 0x40 is already base for this driver */
#define PHY_SHUTDOWNZ_REG		0x00000000
#define DPHY_RSTZ_REG 			0x00000004
#define PHY_RX_REG 				0x00000008
#define PHY_STOPSTATE_REG 		0x0000000C
#define PHY_TEST_CTRL0_REG 		0x00000010
#define PHY_TEST_CTRL1_REG 		0x00000014

/* PHY_TEST_CTRL0_REG */
// When active, performs vendor specific interface initialization.
// Active High.
// Note: This line needs an initial high pulse after power up for analog programmability default values to be preset.
#define PHY_TEST_CTRL0_REG_PHY_TESTCLR_MASK 0x00000001U
#define PHY_TEST_CTRL0_REG_PHY_TESTCLR_BIT 0
#define PHY_TEST_CTRL0_REG_PHY_TESTCLR_SHIFT 0U
// Clock to capture testdin bus contents into the macro, with testen signal controlling the operation selection.
#define PHY_TEST_CTRL0_REG_PHY_TESTCLK_MASK 0x00000002U
#define PHY_TEST_CTRL0_REG_PHY_TESTCLK_BIT 1
#define PHY_TEST_CTRL0_REG_PHY_TESTCLK_SHIFT 1U
/* PHY_TEST_CTRL1_REG */
// Test interface 8-bit data input for programming internal registers and accessing test functionalities.
#define PHY_TEST_CTRL1_REG_PHY_TESTDIN_MASK 0x000000FFU
#define PHY_TEST_CTRL1_REG_PHY_TESTDIN_SHIFT 0U
// Vendor-specific 8-bit data output for reading data and other probing functionalities.
#define PHY_TEST_CTRL1_REG_PHY_TESTDOUT_MASK 0x0000FF00U
#define PHY_TEST_CTRL1_REG_PHY_TESTDOUT_SHIFT 8U
// When asserted high, it configures an address write operation on the falling edge of testclk. When asserted low, it configures a data write operation on the rising edge of testclk.
#define PHY_TEST_CTRL1_REG_PHY_TESTEN_MASK 0x00010000U
#define PHY_TEST_CTRL1_REG_PHY_TESTEN_BIT 16
#define PHY_TEST_CTRL1_REG_PHY_TESTEN_SHIFT 16U
/* PHY_STOPSTATE_REG */
// Data lane 0 in Stop state.
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_0_MASK 0x00000001U
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_0_BIT 0
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_0_SHIFT 0U
// Data lane 1 in Stop state
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_1_MASK 0x00000002U
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_1_BIT 1
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_1_SHIFT 1U
// Data lane 2 in Stop state
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_2_MASK 0x00000004U
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_2_BIT 2
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_2_SHIFT 2U
// Data lane 3 in Stop state
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_3_MASK 0x00000008U
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_3_BIT 3
#define PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_3_SHIFT 3U
// D-PHY Clock lane in Stop state
#define PHY_STOPSTATE_REG_PHY_STOPSTATECLK_MASK 0x00010000U
#define PHY_STOPSTATE_REG_PHY_STOPSTATECLK_BIT 16
#define PHY_STOPSTATE_REG_PHY_STOPSTATECLK_SHIFT 16U
// Active Low. This signal indicates that D-PHY Clock Lane module has entered the Ultra Low Power state
#define PHY_RX_REG_PHY_RXULPSCLKNOT_MASK 0x00010000U
#define PHY_RX_REG_PHY_RXULPSCLKNOT_BIT 16
#define PHY_RX_REG_PHY_RXULPSCLKNOT_SHIFT 16U
// Indicates that D-PHY clock lane is actively receiving a DDR clock
#define PHY_RX_REG_PHY_RXCLKACTIVEHS_MASK 0x00020000U
#define PHY_RX_REG_PHY_RXCLKACTIVEHS_BIT 17
#define PHY_RX_REG_PHY_RXCLKACTIVEHS_SHIFT 17U
// This can only be updated when the PHY lane is in stopstate.Number of active data lanes:
#define N_LANES_REG_N_LANES_MASK 0x00000007U
#define N_LANES_REG_N_LANES_SHIFT 0U

/* PHY_SHUTDOWNZ_REG */
// Shutdown input. This line is used to place the complete macro in power down. All analog blocks are in power down mode and digital logic is cleared. Active Low.
#define PHY_SHUTDOWNZ_REG_PHY_SHUTDOWNZ_MASK 0x00000001U
#define PHY_SHUTDOWNZ_REG_PHY_SHUTDOWNZ_BIT 0
#define PHY_SHUTDOWNZ_REG_PHY_SHUTDOWNZ_SHIFT 0U
 
/* DPHY_RSTZ_REG */
// PHY reset output. Active Low.
#define DPHY_RSTZ_REG_DPHY_RSTZ_MASK 0x00000001U
#define DPHY_RSTZ_REG_DPHY_RSTZ_BIT 0
#define DPHY_RSTZ_REG_DPHY_RSTZ_SHIFT 0U

/* CSI2_RESETN_REG */
#define CSI2_RESETN_REG 0x00000008
// DWC_mipi_csi2_host reset output. Active Low.
#define CSI2_RESETN_REG_CSI2_RESETN_MASK 0x00000001U
#define CSI2_RESETN_REG_CSI2_RESETN_BIT 0
#define CSI2_RESETN_REG_CSI2_RESETN_SHIFT 0U


#define VC_EXTENSION_REG 0x000000C8
/* VC_EXTENSION_REG */
// This field indicates status of Virtual Channel Extension:
// -0: Virtual channel extension is enable
// -1: Legacy mode. Virtual channel extension is disabled
#define VC_EXTENSION_REG_VCX_MASK 0x00000001U
#define VC_EXTENSION_REG_VCX_BIT 0
#define VC_EXTENSION_REG_VCX_SHIFT 0U

#define SCRAMBLING_REG 0x00000300
/* SCRAMBLING_REG */
// Enables data de-scrambling on the controller side.
#define SCRAMBLING_REG_SCRAMBLE_ENABLE_MASK 0x00000001U
#define SCRAMBLING_REG_SCRAMBLE_ENABLE_BIT 0
#define SCRAMBLING_REG_SCRAMBLE_ENABLE_SHIFT 0U


#define POLL_RETRIES_STOPSTATE 1000

#define poll(POLL_RETRIES, POLL_CONDITION)			\
	{                                               \
		int poll_retries = 0;                       \
		do {                                        \
			if (++poll_retries >= POLL_RETRIES)     \
				return -1;                          \
			udelay(10);  	                        \
		} while (POLL_CONDITION);                   \
	}

#define CSI_OFFSET 0x00000000
#define APB_OFFSET 0x00010000

#define apb_read(_dev, addr) dw_read(_dev, (APB_OFFSET + addr))
#define apb_write(_dev, addr, val) dw_write(_dev, (APB_OFFSET + addr), val)
#define apb_modify(_dev, addr, msk, val)                                                       \
	dw_modify(_dev, (APB_OFFSET + addr), addr##_##msk##_MASK, (val) << addr##_##msk##_SHIFT)
#define apb_clear(_dev, addr, bitnum)                                                          \
	dw_clear_bit(_dev, (APB_OFFSET + addr), addr##_##bitnum##_BIT)
#define apb_set(_dev, addr, bitnum) dw_set_bit(_dev, (APB_OFFSET + addr), addr##_##bitnum##_BIT)

#define csi_read(_dev, addr) dw_read(_dev, (CSI_OFFSET + addr))
#define csi_write(_dev, addr, val) dw_write(_dev, (CSI_OFFSET + addr), val)
#define csi_modify(_dev, addr, msk, val)                                                       \
	dw_modify(_dev, (CSI_OFFSET + addr), addr##_##msk##_MASK, (val) << addr##_##msk##_SHIFT)
#define csi_clear(_dev, addr, bitnum)                                                          \
	dw_clear_bit(_dev, (CSI_OFFSET + addr), addr##_##bitnum##_BIT)
#define csi_set(_dev, addr, bitnum) dw_set_bit(_dev, (CSI_OFFSET + addr), addr##_##bitnum##_BIT)


struct freq_range {
	uint16_t frequency;
	uint8_t hs_freq_range;
	uint16_t osc_freq_target;
};

/* Table 7-8 Frequency Ranges and Defaults */
static const struct freq_range freq_ranges[] = {
	{   80, 0x00, 489 }, {   90, 0x10, 489 }, {  100, 0x20, 489 },
	{  110, 0x30, 489 }, {  120, 0x01, 489 }, {  130, 0x11, 489 },
	{  140, 0x21, 489 }, {  150, 0x31, 489 }, {  160, 0x02, 489 },
	{  170, 0x12, 489 }, {  180, 0x22, 489 }, {  190, 0x32, 489 },
	{  205, 0x03, 489 }, {  220, 0x13, 489 }, {  235, 0x23, 489 },
	{  250, 0x33, 489 }, {  275, 0x04, 489 }, {  300, 0x14, 489 },
	{  325, 0x25, 489 }, {  350, 0x35, 489 }, {  400, 0x05, 489 },
	{  450, 0x16, 489 }, {  500, 0x26, 489 }, {  550, 0x37, 489 },
	{  600, 0x07, 489 }, {  650, 0x18, 489 }, {  700, 0x28, 489 },
	{  750, 0x39, 489 }, {  800, 0x09, 489 }, {  850, 0x19, 489 },
	{  900, 0x29, 489 }, {  950, 0x3A, 489 }, { 1000, 0x0A, 489 },
	{ 1050, 0x1A, 489 }, { 1100, 0x2A, 489 }, { 1150, 0x3B, 489 },
	{ 1200, 0x0B, 489 }, { 1250, 0x1B, 489 }, { 1300, 0x2B, 489 },
	{ 1350, 0x3C, 489 }, { 1400, 0x0C, 489 }, { 1450, 0x1C, 489 },
	{ 1500, 0x2C, 489 }, { 1550, 0x3D, 303 }, { 1600, 0x0D, 313 },
	{ 1650, 0x1D, 323 }, { 1700, 0x2E, 333 }, { 1750, 0x3E, 342 },
	{ 1800, 0x0E, 352 }, { 1850, 0x1E, 362 }, { 1900, 0x2F, 372 },
	{ 1950, 0x3F, 381 }, { 2000, 0x0F, 391 }, { 2050, 0x40, 401 },
	{ 2100, 0x41, 411 }, { 2150, 0x42, 411 }, { 2200, 0x43, 411 },
	{ 2250, 0x44, 411 }, { 2300, 0x45, 411 }, { 2350, 0x46, 411 },
	{ 2400, 0x47, 411 }, { 2450, 0x48, 411 }, { 2500, 0x49, 411 }
};


/* remove the phy offset to be able to control the registers of the csi controller */
void dw_csi_write(const struct dw_dphy_rx *dphy, uint32_t address, uint32_t data)
{
	iowrite32(data, dphy->base_address - 0x40 + address);
	return;
}
/* remove the phy offset to be able to control the registers of the csi controller */
uint32_t dw_csi_read(const struct dw_dphy_rx *dphy, uint32_t address)
{
	return ioread32(dphy->base_address - 0x40 + address);
}

void dw_write(const struct dw_dphy_rx *dphy, uint32_t address, uint32_t data)
{
	iowrite32(data, dphy->base_address + address);
	return;
}

uint32_t dw_read(const struct dw_dphy_rx *dphy, uint32_t address)
{
	return ioread32(dphy->base_address + address);
}

void dw_modify(const struct dw_dphy_rx *dphy, u16 addr, uint8_t mask, uint8_t value)
{
	uint32_t tmp = dw_read(dphy, addr);

	tmp &= ~mask;
	tmp |= value;

	dw_write(dphy, addr, tmp);
}

void dw_set_bit(const struct dw_dphy_rx *dphy, uint32_t address, uint32_t bitnum)
{
	uint32_t val = dw_read(dphy, address);
	val |= BIT(bitnum);
	dw_write(dphy,  address, val);
}

void dw_clear_bit(const struct dw_dphy_rx *dphy, uint32_t address, uint32_t bitnum)
{
	uint32_t val = dw_read(dphy, address);
	val &= ~BIT(bitnum);
	dw_write(dphy,  address, val);
}

void dw_modify_tc(const struct dw_dphy_rx *dphy, u16 addr, uint8_t mask, uint8_t value);

void dw_write_tc(const struct dw_dphy_rx *dphy, u16 addr, uint8_t val)
{
	// writing 4-bit test code MSB
	csi_clear(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_set(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_set(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_modify(dphy, PHY_TEST_CTRL1_REG, PHY_TESTDIN, 0x00);

	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_clear(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_modify(dphy, PHY_TEST_CTRL1_REG, PHY_TESTDIN, (addr >> 8) & 0x0f);
	csi_set(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);

	// writing 8-bit test code LSB
	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_set(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_set(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_modify(dphy, PHY_TEST_CTRL1_REG, PHY_TESTDIN, addr & 0xff);
	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_clear(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);

	// writing data
	csi_modify(dphy, PHY_TEST_CTRL1_REG, PHY_TESTDIN, val);
	csi_set(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);

	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
}

uint8_t dw_read_tc(const struct dw_dphy_rx *dphy, u16 addr)
{
	uint8_t val;

	// writing 4-bit test code MSB
	csi_clear(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_set(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_set(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_modify(dphy, PHY_TEST_CTRL1_REG, PHY_TESTDIN, 0x00);
	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_clear(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_modify(dphy, PHY_TEST_CTRL1_REG, PHY_TESTDIN, (addr >> 8) & 0xff);
	csi_set(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);

	// writing 8-bit test code LSB
	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_set(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);
	csi_set(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);
	csi_modify(dphy, PHY_TEST_CTRL1_REG, PHY_TESTDIN, addr & 0xff);
	csi_clear(dphy, PHY_TEST_CTRL0_REG, PHY_TESTCLK);

	// writing dummy data, read value
	csi_write(dphy, PHY_TEST_CTRL1_REG, 0x00);
	val = csi_read(dphy, PHY_TEST_CTRL1_REG) >> 8;
	csi_clear(dphy, PHY_TEST_CTRL1_REG, PHY_TESTEN);

	return val;
}

void dw_modify_tc(const struct dw_dphy_rx *dphy, u16 addr, uint8_t mask, uint8_t value)
{
	uint32_t tmp = dw_read_tc(dphy, addr);

	tmp &= ~mask;
	tmp |= value;

	dw_write_tc(dphy, addr, tmp);
}

void dw_phy_release_tc(const struct dw_dphy_rx *dphy)
{
	csi_write(dphy, PHY_TEST_CTRL1_REG, 0);
	csi_write(dphy, PHY_TEST_CTRL0_REG, 1);
	csi_write(dphy, PHY_TEST_CTRL0_REG, 0);
}


/***********************************************************************//**
 * test
 ***************************************************************************/
static int dw_dphy_wait_stop_state(const struct dw_dphy_rx *dphy)
{
#if 1
	/*
    // wait for stop state clk
    */
	poll(POLL_RETRIES_STOPSTATE,
	     ((csi_read(dphy, PHY_STOPSTATE_REG) & PHY_STOPSTATE_REG_PHY_STOPSTATECLK_MASK)
	      != PHY_STOPSTATE_REG_PHY_STOPSTATECLK_MASK));
	/*
    // wait for stop state lanes 1/2/3/4
    */
	// wait for stop state lane 0
	poll(POLL_RETRIES_STOPSTATE,
	     ((csi_read(dphy, PHY_STOPSTATE_REG) & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_0_MASK)
	      != PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_0_MASK));
	
	// wait for stop state lane 1
	//if (rt->cfg.n_lanes > 1)
		poll(POLL_RETRIES_STOPSTATE, ((csi_read(dphy, PHY_STOPSTATE_REG)
					       & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_1_MASK)
					      != PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_1_MASK));
	// wait for stop state lane 2
	//if (rt->cfg.n_lanes > 2)
		poll(POLL_RETRIES_STOPSTATE, ((csi_read(dphy, PHY_STOPSTATE_REG)
					       & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_2_MASK)
					      != PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_2_MASK));
	// wait for stop state lane 3
	//if (rt->cfg.n_lanes > 3)
		poll(POLL_RETRIES_STOPSTATE, ((csi_read(dphy, PHY_STOPSTATE_REG)
					       & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_3_MASK)
					      != PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_3_MASK));

	return 0;
#else
	
	uint32_t exp_status, cur_status;
	uint32_t try = 0;

    exp_status = PHY_STOPSTATE_REG_PHY_STOPSTATECLK_MASK 
	           | PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_0_MASK
			   | PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_1_MASK
			   | PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_2_MASK
			   | PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_3_MASK;

    do {
        try++;
		cur_status = csi_read(dphy, PHY_STOPSTATE_REG); 
    } while (cur_status != exp_status);
	
	printk("Stopstate current Status 0x%0x(0x%0x) after %d tries", cur_status, exp_status, try);
	return 0;
	
#endif
}

static bool dw_dphy_is_stop_state(const struct dw_dphy_rx *dphy, int lane)
{
	uint32_t reg = csi_read(dphy, PHY_STOPSTATE_REG);

	switch (lane) {
	case 1: // lane 1
		return !!(reg & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_0_MASK);
	case 2: // lane 2
		return !!(reg & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_1_MASK);
	case 3: // lane 3
		return !!(reg & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_2_MASK);
	case 4: // lane 4
		return !!(reg & PHY_STOPSTATE_REG_PHY_STOPSTATEDATA_3_MASK);
	case 0: // clk lane
	default:
		return !!(reg & PHY_STOPSTATE_REG_PHY_STOPSTATECLK_MASK);
	}
}

static bool dw_dphy_is_high_speed(const struct dw_dphy_rx *dphy)
{
	return !!(csi_read(dphy, PHY_RX_REG) & PHY_RX_REG_PHY_RXCLKACTIVEHS_MASK);
}

static bool dw_dphy_is_ulpm(const struct dw_dphy_rx *dphy)
{
	uint32_t val = csi_read(dphy, PHY_RX_REG);
	return ((val & 0xff) == 0xff) && ((val & PHY_RX_REG_PHY_RXULPSCLKNOT_MASK) == 0);
}

static int dw_phy_get_range(uint32_t frequency)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(freq_ranges); i++) {
		if (frequency > freq_ranges[i].frequency) {
			continue;
		}
		return i;
	}

	return -1;
}

static int dw_phy_up(struct dw_dphy_rx *dphy)
{
	int ret;
	uint8_t hs_freq_range;
	uint16_t osc_freq_target;
	uint32_t mipi_rx_rb_ctrl0 = 0;
	uint32_t mipi_rx_rb_ctrl1 = 0;
	uint32_t mipi_rx_rb_ctrl2 = 0;
	uint32_t mipi_rx_rb_ctrl3 = 0;

	// get configuration
	ret = dw_phy_get_range(1600);//@TODO fixme, hardcoded
	if (ret == -1) {
		printk("dw_phy_get_range returns error -1");
		return -1;
	}
	hs_freq_range = freq_ranges[ret].hs_freq_range;
	osc_freq_target = freq_ranges[ret].osc_freq_target;

	// Make sure PHY is off before configuration starts and hold controller in reset
	dw_csi_write(dphy, CSI2_RESETN_REG, 0);

	csi_clear(dphy, DPHY_RSTZ_REG, DPHY_RSTZ);
	csi_clear(dphy, PHY_SHUTDOWNZ_REG, PHY_SHUTDOWNZ);

	udelay(1000);

	// Configure 4 active lanes
	dw_csi_write(dphy, N_LANES_REG, 3);
	// Disable VCX
	dw_csi_write(dphy, VC_EXTENSION_REG, 0x00);
	// Disable Data De-Scrambling
	dw_csi_write(dphy, SCRAMBLING_REG, 0x00);


	//set Test IF to RX
	// dphy->zuk_mipi_rx->mipi_rx_rb_ctrl0 &= ~ZUK_MIPI_RX_RB_BASE_TIF_SEL_MASK;
	// dw_phy_release_tc(dphy);	// Reset PHY test codes

	//set Test IF to TX
	mipi_rx_rb_ctrl0 = isp_pre_read(dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL0_REG);
	DCT_REGS_FLAG_SET(mipi_rx_rb_ctrl0, TIF_SEL);
	isp_pre_write(mipi_rx_rb_ctrl0, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL0_REG);
	dw_phy_release_tc(dphy);	// Reset PHY test codes

	// Configure HSFREQRANGE
	mipi_rx_rb_ctrl1 = isp_pre_read(dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL1_REG);
	DCT_REGS_FIELD_SET(mipi_rx_rb_ctrl1, HSFREQRANGE, hs_freq_range);
	isp_pre_write(hs_freq_range, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL1_REG);

	//set Test IF to TX
 	//dphy->zuk_mipi_rx->mipi_rx_rb_ctrl0 |= ZUK_MIPI_RX_RB_BASE_TIF_SEL_MASK;

	//set Test IF to TX
 	//dphy->zuk_mipi_rx->mipi_rx_rb_ctrl0 |= ZUK_MIPI_RX_RB_BASE_TIF_SEL_MASK;
	dw_write_tc(dphy, 0x16a, 0x3);	// bypass internal regulator: mpll_vreg_clout, mpll_vreg1p2
	dw_write_tc(dphy, 0x1ab, 0x2);	// LPRX voltage reference 325mV
	dw_write_tc(dphy, 0x1aa, 0x40); // LPRX connection detector voltage reference 325mV

	dw_write_tc(dphy, 0x26b, 0x0c); // bypass slew reg calib. during phy power up (overule FSM state 7?)

	//set Test IF to RX
	DCT_REGS_FLAG_CLEAR(mipi_rx_rb_ctrl0, TIF_SEL);
	isp_pre_write(mipi_rx_rb_ctrl0, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL0_REG);
	dw_phy_release_tc(dphy);	// Reset PHY test codes
	dw_write_tc(dphy, 0x307, 0x80);

	// Configure osc_freq_target override
	dw_write_tc(dphy, RDWR_RX_RX_STARTUP_OVR_2, (osc_freq_target)&0xff);
	dw_write_tc(dphy, RDWR_RX_RX_STARTUP_OVR_3, (osc_freq_target >> 8) & 0x0f);
	dw_write_tc(dphy, RDWR_RX_RX_STARTUP_OVR_4, DDL_OSC_FREQ_TARGET_OVR_EN_MASK);

	// RF_CFGCLKFREQRANGE: round[(Fcfg_clk(MHz)-17)*4], Fcfg_clk = 25MHz
	DCT_REGS_FIELD_SET(mipi_rx_rb_ctrl0, CFGCLKFREQRANGE, 0x20 );
	isp_pre_write(mipi_rx_rb_ctrl0, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL0_REG);

	// counter_for_des_en configuration
	dw_modify_tc(dphy, RDWR_RX_RX_STARTUP_OVR_5, 0x01, 0x01);
	dw_modify_tc(dphy, RDWR_RX_RX_STARTUP_OVR_4, 0xF0, 0x00);

	dw_modify_tc(dphy, RDWR_RX_CB_1, 0x03, 0x02);
	dw_modify_tc(dphy, RDWR_RX_CB_2, 0x40, 0x40);
	dw_modify_tc(dphy, RDWR_RX_CLKLANE_LANE_4, 0x0f,
		     0x0f); // for CTS 2.2.3: Synopsys proposed fix to pass ULP test
	dw_modify_tc(dphy, RDWR_RX_CLKLANE_LANE_6, 0x80, 0x80);

	if (1600/*rt->cfg.data_rate*/ < 1500) {
		// Enable D-PHY 1.1 compatibility mode
		dw_modify_tc(dphy, RDWR_RX_SYS_7, 0x02, 0x02);
		dw_modify_tc(dphy, RDWR_RX_LANE0_DDL_1, 0x9f, 0x9f);
		//if (rt->cfg.n_lanes > 1)
			dw_modify_tc(dphy, RDWR_RX_LANE1_DDL_1, 0x9f, 0x9f);
		//if (rt->cfg.n_lanes > 2)
			dw_modify_tc(dphy, RDWR_RX_LANE2_DDL_1, 0x9f, 0x9f);
		//if (rt->cfg.n_lanes > 3)
			dw_modify_tc(dphy, RDWR_RX_LANE3_DDL_1, 0x9f, 0x9f);
	} else {
		// Configure D-PHY 1.2 deskew calibration
		dw_modify_tc(dphy, RDWR_RX_SYS_9, 0xFF, 0x43);
	}

	// always overwrite tclk_miss_div2_en
	dw_modify_tc(dphy, RDWR_RX_SYS_7, 0xC0, 0xC0);

	// Configure cfgclkfreqrange, through APB reg interface
	// cfg_clk should always be 20MHz for Hamburg usecase
	// f(cfg_clk) = 20MHz => round((20-17)*4)=12
	//apb_modify(dphy, FREQRANGE_CFG_REG, CFGCLKFREQRANGE, 12);



	//apb_set(dphy, PHY_EXTENDED_CTRL0_REG, BASEDIR_0);

	// value = apb_read(dphy, PHY_EXTENDED_CTRL0_REG);
	// value |= PHY_EXTENDED_CTRL0_REG_FORCERXMODE_0_MASK;
	// if (rt->cfg.n_lanes > 1)
	// 	value |= PHY_EXTENDED_CTRL0_REG_FORCERXMODE_1_MASK;
	// if (rt->cfg.n_lanes > 2)
	// 	value |= PHY_EXTENDED_CTRL0_REG_FORCERXMODE_2_MASK;
	// if (rt->cfg.n_lanes > 3)
	// 	value |= PHY_EXTENDED_CTRL0_REG_FORCERXMODE_3_MASK;
	// apb_write(dphy, PHY_EXTENDED_CTRL0_REG, value);

	// apb_set(dphy, PHY_EXTENDED_CTRL0_REG, ENABLECLK);


	DCT_REGS_FLAG_SET(mipi_rx_rb_ctrl3, BASEDIR_0);
	isp_pre_write(mipi_rx_rb_ctrl3, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL3_REG);
	// Force RX mode before releasing resets ZUK_MIPI_RX_RB_BASE_FORCERXMODE_SHIFT
	DCT_REGS_FIELD_SET(mipi_rx_rb_ctrl2, FORCERXMODE, 0x0F);
	isp_pre_write(mipi_rx_rb_ctrl2, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL2_REG);
	DCT_REGS_FLAG_SET(mipi_rx_rb_ctrl3, ENABLECLK);
	isp_pre_write(mipi_rx_rb_ctrl3, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL3_REG);

	// release PHY reset
	csi_set(dphy, PHY_SHUTDOWNZ_REG, PHY_SHUTDOWNZ);
	csi_set(dphy, DPHY_RSTZ_REG, DPHY_RSTZ);

	dev_vdbg(&dphy->phy->dev,
			"Before Stop HS Code: ovr_freq:0x%x mux_freq:0x%x ext_freq:0x%x herby_freq:0x%x\n", 
			dw_read_tc(dphy, RX_SYS_1),
			dw_read_tc(dphy, 0xcb),
			dw_read_tc(dphy, 0x1f),
			isp_pre_read(dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL1_REG));
	//wait_mipicsi2_host_stop_state(ctrl_regs, cfg->lane_active_num);
	ret = dw_dphy_wait_stop_state(dphy);
	if (ret) {
		printk("dw_phy_up: wait for stop state failed (0x%0x)", csi_read(dphy, PHY_STOPSTATE_REG));
		return -1;
	}

	// release forcerx only after stop state has been reached
	// value = apb_read(dphy, PHY_EXTENDED_CTRL0_REG);
	// value &= ~(PHY_EXTENDED_CTRL0_REG_FORCERXMODE_0_MASK
	// 	   | PHY_EXTENDED_CTRL0_REG_FORCERXMODE_1_MASK
	// 	   | PHY_EXTENDED_CTRL0_REG_FORCERXMODE_2_MASK
	// 	   | PHY_EXTENDED_CTRL0_REG_FORCERXMODE_3_MASK);
	// apb_write(dphy, PHY_EXTENDED_CTRL0_REG, value);

	// remove RX mode force
	DCT_REGS_FIELD_SET(mipi_rx_rb_ctrl2, FORCERXMODE, 0x00);
	isp_pre_write(mipi_rx_rb_ctrl2, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL2_REG);


	// release Host Controller reset
	dw_csi_write(dphy, CSI2_RESETN_REG, 1);

	return 0;
}

/***********************************************************************//**
 * API functions
 ***************************************************************************/
int dct_dphy_init(struct phy *phy)
{
	struct dw_dphy_rx *dphy = phy_get_drvdata(phy);
	dev_vdbg(&dphy->phy->dev, "Init DPHY.\n");

	dw_write(dphy, R_CSI2_DPHY_RSTZ, 0);
	dw_write(dphy, R_CSI2_DPHY_SHUTDOWNZ, 0);

	return 0;
}

int dct_dphy_power_on(struct phy *phy)
{
	struct dw_dphy_rx *dphy = phy_get_drvdata(phy);
	uint8_t hs_freq;

	dev_vdbg(&dphy->phy->dev, "PowerOn DPHY.\n");

	dphy->lanes_config = 4;
	hs_freq = RX_SYS_1;

	dw_phy_up(dphy);

	dev_vdbg(&dphy->phy->dev,
			"HS Code: ovr_freq:0x%x mux_freq:0x%x ext_freq:0x%x herby_freq:0x%x\n", 
			dw_read_tc(dphy, hs_freq),
			dw_read_tc(dphy, 0xcb),
			dw_read_tc(dphy, 0x1f),
			isp_pre_read(dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL1_REG));

	return 0;
}

int dct_dphy_power_off(struct phy *phy)
{
	struct dw_dphy_rx *dphy = phy_get_drvdata(phy);
	dw_write(dphy, R_CSI2_DPHY_SHUTDOWNZ, 0);
	dw_write(dphy, R_CSI2_DPHY_RSTZ, 0);
	return 0;
}

int dct_dphy_reset(struct phy *phy)
{
	struct dw_dphy_rx *dphy = phy_get_drvdata(phy);
	dev_vdbg(&dphy->phy->dev, "Reset DPHY.\n");
	dw_write(dphy, R_CSI2_DPHY_RSTZ, 0);
	usleep_range(1000, 1200);
	dw_write(dphy, R_CSI2_DPHY_RSTZ, 1);

	return 0;
}


static ssize_t dphy_dump_show(struct device *dev,
			       struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dw_dphy_rx *dphy = platform_get_drvdata(pdev);

	char buffer[1024];
	//set Test IF to RX
	uint32_t mipi_rx_rb_ctrl0 = isp_pre_read(dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL0_REG);
	DCT_REGS_FLAG_CLEAR(mipi_rx_rb_ctrl0, TIF_SEL);
	isp_pre_write(mipi_rx_rb_ctrl0, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL0_REG);

	{
		//dw_phy_glue_if_tester(dphy, 0x04);
		uint32_t RDWR_rx_sys_0 = dw_read_tc(dphy, RDWR_RX_SYS_0);
		uint32_t RDWR_rx_sys_1 = dw_read_tc(dphy, RDWR_RX_SYS_1);
		uint32_t RDWR_rx_sys_2 = dw_read_tc(dphy, RDWR_RX_SYS_2);
		uint32_t RDWR_rx_sys_3 = dw_read_tc(dphy, RDWR_RX_SYS_3);
		uint32_t RDWR_rx_sys_4 = dw_read_tc(dphy, RDWR_RX_SYS_4);
		uint32_t RDWR_rx_sys_5 = dw_read_tc(dphy, RDWR_RX_SYS_5);
		uint32_t RDWR_rx_sys_6 = dw_read_tc(dphy, RDWR_RX_SYS_6);
		uint32_t RDWR_rx_sys_7 = dw_read_tc(dphy, RDWR_RX_SYS_7);

		//dw_phy_glue_if_tester(dphy, 0x02);
		uint32_t RDWR_RX_rx_startup_ovr_0 = dw_read_tc(dphy, RDWR_RX_RX_STARTUP_OVR_0);
		uint32_t RDWR_RX_rx_startup_ovr_1 = dw_read_tc(dphy, RDWR_RX_RX_STARTUP_OVR_1);
		uint32_t RDWR_RX_rx_startup_ovr_2 = dw_read_tc(dphy, RDWR_RX_RX_STARTUP_OVR_2);
		uint32_t RDWR_RX_rx_startup_ovr_3 = dw_read_tc(dphy, RDWR_RX_RX_STARTUP_OVR_3);
		uint32_t RDWR_RX_rx_startup_ovr_4 = dw_read_tc(dphy, RDWR_RX_RX_STARTUP_OVR_4);
		uint32_t RDWR_RX_rx_startup_ovr_5 = dw_read_tc(dphy, RDWR_RX_RX_STARTUP_OVR_5);

		uint32_t rx_state_main = dw_read_tc(dphy, 0x1e);

		uint32_t stopstate_clk = dw_dphy_is_stop_state(dphy, 0);
		uint32_t stopstate_0 = dw_dphy_is_stop_state(dphy, 1);
		uint32_t stopstate_1 = dw_dphy_is_stop_state(dphy, 2);
		uint32_t stopstate_2 = dw_dphy_is_stop_state(dphy, 3);
		uint32_t stopstate_3 = dw_dphy_is_stop_state(dphy, 4);
		uint32_t highspeed = dw_dphy_is_high_speed(dphy);
		uint32_t ulpm = dw_dphy_is_ulpm(dphy);


		uint32_t RD_RX_rx_startup_obs_0 = dw_read_tc(dphy, RD_RX_RX_STARTUP_OBS_0);
		uint32_t RD_RX_rx_startup_obs_1 = dw_read_tc(dphy, RD_RX_RX_STARTUP_OBS_1);
		uint32_t RD_RX_rx_startup_obs_2 = dw_read_tc(dphy, RD_RX_RX_STARTUP_OBS_2);
		uint32_t RD_RX_rx_startup_obs_3 = dw_read_tc(dphy, RD_RX_RX_STARTUP_OBS_3);
		uint32_t RD_RX_rx_startup_obs_4 = dw_read_tc(dphy, RD_RX_RX_STARTUP_OBS_4);

		//TX config space
		uint32_t RD_tx_slew_0 = dw_read_tc(dphy, RD_TX_SLEW_0);
		uint32_t RDWR_tx_slew_0 = dw_read_tc(dphy, RDWR_TX_SLEW_0);
		DCT_REGS_FLAG_SET(mipi_rx_rb_ctrl0, TIF_SEL);
		isp_pre_write(mipi_rx_rb_ctrl0, dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL0_REG);

		snprintf(buffer, 1024,
		"RX_STATE_MAIN: 	  0x%02x\n"
		"RDWR_RX_SYS_0:		  0x%02x\n"
		"RDWR_RX_SYS_1:		  0x%02x\n"
		"RDWR_RX_SYS_2:		  0x%02x\n"
		"RDWR_RX_SYS_3:		  0x%02x\n"
		"RDWR_RX_SYS_4:		  0x%02x\n"
		"RDWR_RX_SYS_5:		  0x%02x\n"
		"RDWR_RX_SYS_6:		  0x%02x\n"
		"RDWR_RX_SYS_7:		  0x%02x\n"
		"RDWR_RX_RX_STARTUP_OVR_0: 0x%02x\n"
		"RDWR_RX_RX_STARTUP_OVR_1: 0x%02x\n"
		"RDWR_RX_RX_STARTUP_OVR_2: 0x%02x\n"
		"RDWR_RX_RX_STARTUP_OVR_3: 0x%02x\n"
		"RDWR_RX_RX_STARTUP_OVR_4: 0x%02x\n"
		"RDWR_RX_RX_STARTUP_OVR_5: 0x%02x\n\n"
		"stop state:       %d/%d/%d/%d/%d\n"
		"high speed:       %d\n"
		"ulpm:             %d\n"
		"RD_RX_RX_STARTUP_OBS_0: 0x%02x\n"
		"RD_RX_RX_STARTUP_OBS_1: 0x%02x\n"
		"RD_RX_RX_STARTUP_OBS_2: 0x%02x\n"
		"RD_RX_RX_STARTUP_OBS_3: 0x%02x\n"
		"RD_RX_RX_STARTUP_OBS_4: 0x%02x\n"
		"RD_TX_SLEW_0:           0x%02x\n"
		"RDWR_TX_SLEW_0:         0x%02x\n"
		, rx_state_main
		, RDWR_rx_sys_0
		, RDWR_rx_sys_1
		, RDWR_rx_sys_2
		, RDWR_rx_sys_3
		, RDWR_rx_sys_4
		, RDWR_rx_sys_5
		, RDWR_rx_sys_6
		, RDWR_rx_sys_7
		, RDWR_RX_rx_startup_ovr_0
		, RDWR_RX_rx_startup_ovr_1
		, RDWR_RX_rx_startup_ovr_2
		, RDWR_RX_rx_startup_ovr_3
		, RDWR_RX_rx_startup_ovr_4
		, RDWR_RX_rx_startup_ovr_5
		, stopstate_clk, stopstate_0, stopstate_1, stopstate_2, stopstate_3
		, highspeed
		, ulpm
		, RD_RX_rx_startup_obs_0
		, RD_RX_rx_startup_obs_1
		, RD_RX_rx_startup_obs_2
		, RD_RX_rx_startup_obs_3
		, RD_RX_rx_startup_obs_4
		, RD_tx_slew_0
		, RDWR_tx_slew_0);
	}
	
	return strlcpy(buf, buffer, PAGE_SIZE);
}

static ssize_t dphy_tpg_store(struct device *dev, struct device_attribute *attr,
			     const char *buf, size_t count)
{
	int ret;
	unsigned long enable;

	struct platform_device *pdev = to_platform_device(dev);
	struct dw_dphy_rx *dphy = platform_get_drvdata(pdev);

    dw_csi_read(dphy, CSI2_PPI_PG_STATUS);
	
	ret = kstrtoul(buf, 10, &enable);
	if (ret < 0)
		return ret;

	dw_csi_write(dphy, CSI2_PPI_PG_CONFIG, 0x00002C00);
	dw_csi_write(dphy, CSI2_PPI_PG_ENABLE, enable? 0x1:0x00);
	dev_info(dev, "enable tpg: %s\n",  enable? "on":"off");

	return count;
}

static ssize_t dphy_tpg_show(struct device *dev,
			    struct device_attribute *attr,
			    char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct dw_dphy_rx *dphy = platform_get_drvdata(pdev);
	char buffer[128];

    uint32_t status = dw_csi_read(dphy, CSI2_PPI_PG_STATUS);
	uint32_t enable = dw_csi_read(dphy, CSI2_PPI_PG_ENABLE);

	snprintf(buffer, 128, "tpg\n"
						  "pg_status: 0x%0x\n"
						  "pg_enable: 0x%0x\n", status, enable);

	return strlcpy(buf, buffer, PAGE_SIZE);
}

static DEVICE_ATTR_RO(dphy_dump);
static DEVICE_ATTR_RW(dphy_tpg);

int dct_dphy_create_capabilities_sysfs(struct platform_device *pdev)
{
	device_create_file(&pdev->dev, &dev_attr_dphy_dump);
	device_create_file(&pdev->dev, &dev_attr_dphy_tpg);
	return 0;
}
