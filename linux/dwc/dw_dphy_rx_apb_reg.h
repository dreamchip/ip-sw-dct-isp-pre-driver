/*
 * Copyright (C) 2022 Dream Chip Technologies
 *
 * SPDX-License-Identifier: BSD
 */

#ifndef __DW_DPHY_RX_APB_REG__
#define __DW_DPHY_RX_APB_REG__

#define FREQRANGE_CFG_REG 0x00000800
#define PHY_EXTENDED_CTRL0_REG 0x00000804
#define PHY_EXTENDED_STS0_REG 0x00000808
#define PHY_CONTINUITY_CTRL_REG 0x0000080C
#define PHY_CONTINUITY_STS_REG 0x00000810

/* FREQRANGE_CFG_REG */
// Register field to control PHY input port cfgclkfreqrange.
#define FREQRANGE_CFG_REG_CFGCLKFREQRANGE_MASK 0x0000003FU
#define FREQRANGE_CFG_REG_CFGCLKFREQRANGE_SHIFT 0U
// Register field to control PHY input port hsfreqrange.
#define FREQRANGE_CFG_REG_HSFREQRANGE_MASK 0x00007F00U
#define FREQRANGE_CFG_REG_HSFREQRANGE_SHIFT 8U

/* PHY_EXTENDED_CTRL0_REG */
// Register field to control PHY input port basedir_0.
#define PHY_EXTENDED_CTRL0_REG_BASEDIR_0_MASK 0x00000001U
#define PHY_EXTENDED_CTRL0_REG_BASEDIR_0_BIT 0
#define PHY_EXTENDED_CTRL0_REG_BASEDIR_0_SHIFT 0U
// Register field to control PHY input port forcerxmode_0.
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_0_MASK 0x00000010U
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_0_BIT 4
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_0_SHIFT 4U
// Register field to control PHY input port forcerxmode_1.
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_1_MASK 0x00000020U
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_1_BIT 5
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_1_SHIFT 5U
// Register field to control PHY input port forcerxmode_2.
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_2_MASK 0x00000040U
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_2_BIT 6
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_2_SHIFT 6U
// Register field to control PHY input port forcerxmode_3.
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_3_MASK 0x00000080U
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_3_BIT 7
#define PHY_EXTENDED_CTRL0_REG_FORCERXMODE_3_SHIFT 7U
// Register field to control PHY input port enableclk.
#define PHY_EXTENDED_CTRL0_REG_ENABLECLK_MASK 0x00000100U
#define PHY_EXTENDED_CTRL0_REG_ENABLECLK_BIT 8
#define PHY_EXTENDED_CTRL0_REG_ENABLECLK_SHIFT 8U

/* PHY_EXTENDED_STS0_REG */
// Register field reports if PHY output port errsyncesc _0 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_0_MASK 0x00000001U
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_0_BIT 0
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_0_SHIFT 0U
// Register field reports if PHY output port errsyncesc _1 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_1_MASK 0x00000002U
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_1_BIT 1
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_1_SHIFT 1U
// Register field reports if PHY output port errsyncesc _2 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_2_MASK 0x00000004U
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_2_BIT 2
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_2_SHIFT 2U
// Register field reports if PHY output port errsyncesc _3 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_3_MASK 0x00000008U
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_3_BIT 3
#define PHY_EXTENDED_STS0_REG_ERRSYNCESC_3_SHIFT 3U
// Register field reports if PHY output port errcontrol_0 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_0_MASK 0x00000010U
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_0_BIT 4
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_0_SHIFT 4U
// Register field reports if PHY output port errcontrol_1 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_1_MASK 0x00000020U
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_1_BIT 5
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_1_SHIFT 5U
// Register field reports if PHY output port errcontrol_2 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_2_MASK 0x00000040U
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_2_BIT 6
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_2_SHIFT 6U
// Register field reports if PHY output port errcontrol_3 has been asserted. Reading this register clears it.
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_3_MASK 0x00000080U
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_3_BIT 7
#define PHY_EXTENDED_STS0_REG_ERRCONTROL_3_SHIFT 7U

/* PHY_CONTINUITY_CTRL_REG */
// Register field to control mux of PHY cont_en signal. If asserted cont_en is controlled by top-level IO.
#define PHY_CONTINUITY_CTRL_REG_CONT_EXT_MASK 0x00000001U
#define PHY_CONTINUITY_CTRL_REG_CONT_EXT_BIT 0
#define PHY_CONTINUITY_CTRL_REG_CONT_EXT_SHIFT 0U
// Register field to control PHY input port cont_en.
#define PHY_CONTINUITY_CTRL_REG_CONT_EN_MASK 0x00000010U
#define PHY_CONTINUITY_CTRL_REG_CONT_EN_BIT 4
#define PHY_CONTINUITY_CTRL_REG_CONT_EN_SHIFT 4U

/* PHY_CONTINUITY_STS_REG */
// Register field to control mux of PHY cont_en signal. If asserted cont_en is controlled by top-level IO.
#define PHY_CONTINUITY_STS_REG_CONT_DATA_MASK 0x000007FFU
#define PHY_CONTINUITY_STS_REG_CONT_DATA_SHIFT 0U

#endif /* __DW_DPHY_RX_APB_REG__ */
