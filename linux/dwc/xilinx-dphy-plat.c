// SPDX-License-Identifier: GPL-2.0
/*
 *
 */
//#define DEBUG
//#define VERBOSE_DEBUG
#define BEEP printk("%s:%d\n", __func__, __LINE__);

#include "dw-dphy-data.h"
#include "dw-csi-data.h"

#include <linux/delay.h>
#include <linux/phy/phy.h>
#include <linux/phy/phy-mipi-dphy.h>
#include <linux/platform_device.h>

#include "dct_reg_helper.h"
#include "zuk_mipi_rx_rb_regs.h"

/* DPHY interface register bank*/
#define R_CSI2_DPHY_RSTZ 0x4

struct xilinx_dphy_rx {
	struct phy *phy;
	struct phy_configure_opts_mipi_dphy *cfg;
	u32 lanes_config;
	u32 dphy_freq;
	u32 phy_type;
	u32 dphy_gen;
	u32 dphy_te_len;
	u32 max_lanes;
	u32 lp_time;
	void __iomem *base_address;
	uint32_t __iomem *zuk_mipi_rx;
	u8 (*phy_register)(struct device *dev);
	void (*phy_unregister)(struct device *dev);
};

struct xilinx_phy_pdata {
	u32 dphy_frequency;
	u8 dphy_te_len;
	u32 config_8l;
	u8 dphy_gen;
	u8 phy_type;
	u8 id;
};

static void xilinx_dphy_write(struct xilinx_dphy_rx *dphy, u32 address, u32 data)
{
	iowrite32(data, dphy->base_address + address);
}

static inline struct phy *xilinx_dphy_xlate(struct device *dev,
					struct of_phandle_args *args)
{
	struct xilinx_dphy_rx *dphy = dev_get_drvdata(dev);

	return dphy->phy;
}

int xilinx_dphy_init(struct phy *phy)
{
	struct xilinx_dphy_rx *dphy = phy_get_drvdata(phy);

	dev_vdbg(&dphy->phy->dev, "Init xilinx DPHY.\n");

	xilinx_dphy_write(dphy, R_CSI2_DPHY_RSTZ, 0);

	return 0;
}

int xilinx_dphy_power_on(struct phy *phy)
{
	struct xilinx_dphy_rx *dphy = phy_get_drvdata(phy);
	uint32_t mipi_rx_rb_ctrl2 = 0;
	uint32_t mipi_rx_rb_ctrl3 = 0;

	dev_vdbg(&dphy->phy->dev, "%s\n", __func__);

	// Force RX mode before releasing resets
	DCT_REGS_FIELD_SET(mipi_rx_rb_ctrl2, FORCERXMODE, 0x0F);
	writel(mipi_rx_rb_ctrl2, (uintptr_t)dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL2_REG);
	DCT_REGS_FLAG_SET(mipi_rx_rb_ctrl3, ENABLECLK);
	writel(mipi_rx_rb_ctrl3, (uintptr_t)dphy->zuk_mipi_rx + MIPI_RX_RB_CTRL3_REG);

	xilinx_dphy_write(dphy, R_CSI2_DPHY_RSTZ, 1);

	return 0;
}

int xilinx_dphy_power_off(struct phy *phy)
{
	struct xilinx_dphy_rx *dphy = phy_get_drvdata(phy);

	dev_vdbg(&dphy->phy->dev, "%s\n", __func__);

	xilinx_dphy_write(dphy, R_CSI2_DPHY_RSTZ, 0);

	return 0;
}

int xilinx_dphy_reset(struct phy *phy)
{
	struct xilinx_dphy_rx *dphy = phy_get_drvdata(phy);

	dev_vdbg(&dphy->phy->dev, "%s\n", __func__);

	xilinx_dphy_write(dphy, R_CSI2_DPHY_RSTZ, 0);
	usleep_range(100, 200);
	xilinx_dphy_write(dphy, R_CSI2_DPHY_RSTZ, 1);

	return 0;
}

static struct phy_ops xilinx_dphy_ops = {
	.init = xilinx_dphy_init,
	.reset = xilinx_dphy_reset,
	.power_on = xilinx_dphy_power_on,
	.power_off = xilinx_dphy_power_off,
	.owner = THIS_MODULE,
};

static struct phy_provider *phy_provider;

static int phy_register(struct device *dev)
{
	int ret = 0;

	if (IS_ENABLED(CONFIG_OF) && dev->of_node) {
		phy_provider = devm_of_phy_provider_register(dev,
		 					     xilinx_dphy_xlate);
		if (IS_ERR(phy_provider)) {
			dev_err(dev, "error getting phy provider\n");
			ret = PTR_ERR(phy_provider);
		}
	} else {
		struct xilinx_phy_pdata *pdata = dev->platform_data;
		struct xilinx_dphy_rx *dphy = dev_get_drvdata(dev);

		ret = phy_create_lookup(dphy->phy,
					phys[pdata->id].name,
					csis[pdata->id].name);
		if (ret)
			dev_err(dev, "Failed to create dphy lookup\n");
		else
			dev_warn(dev,
				 "Created dphy lookup [%s] --> [%s]\n",
				 phys[pdata->id].name, csis[pdata->id].name);
	}
	return ret;
}

static void phy_unregister(struct device *dev)
{
	if (!dev->of_node) {
		struct xilinx_dphy_rx *dphy = dev_get_drvdata(dev);

		phy_remove_lookup(dphy->phy, "xilinx-dphy", "dw-csi");
	}
}

static int xilinx_dphy_rx_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct xilinx_dphy_rx *dphy;
	struct resource *res;

	dphy = devm_kzalloc(&pdev->dev, sizeof(*dphy), GFP_KERNEL);
	if (!dphy)
		return -ENOMEM;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	dphy->base_address = devm_ioremap(&pdev->dev,
					  res->start, resource_size(res));
	if (IS_ERR(dphy->base_address)) {
		dev_err(&pdev->dev, "error requesting base address\n");
		return PTR_ERR(dphy->base_address);
	}

	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	dphy->zuk_mipi_rx = devm_ioremap(&pdev->dev,
					  res->start, resource_size(res));
	if (IS_ERR(dphy->zuk_mipi_rx)) {
		dev_err(&pdev->dev, "error requesting second base address\n");
		return PTR_ERR(dphy->zuk_mipi_rx);
	}

	dphy->phy = devm_phy_create(dev, NULL, &xilinx_dphy_ops);
	if (IS_ERR(dphy->phy)) {
		dev_err(dev, "failed to create PHY\n");
		return PTR_ERR(dphy->phy);
	}

	platform_set_drvdata(pdev, dphy);
	phy_set_drvdata(dphy->phy, dphy);

	if (phy_register(dev)) {
		dev_err(dev, "failed to register PHY\n");
		return -EINVAL;
	}

	dev_info(dev, "Probing dphy finished\n");

	return 0;
}

static int xilinx_dphy_rx_remove(struct platform_device *pdev)
{
	phy_unregister(&pdev->dev);

	return 0;
}

#if IS_ENABLED(CONFIG_OF)
static const struct of_device_id xilinx_dphy_rx_of_match[] = {
	{ .compatible = "xilinx,xilinx-dphy-rx" },
	{},
};

MODULE_DEVICE_TABLE(of, xilinx_dphy_rx_of_match);
#endif

static struct platform_driver xilinx_dphy_rx_driver = {
	.probe = xilinx_dphy_rx_probe,
	.remove = xilinx_dphy_rx_remove,
	.driver = {
#if IS_ENABLED(CONFIG_OF)
		.of_match_table = of_match_ptr(xilinx_dphy_rx_of_match),
#endif
		.name = "xilinx-dphy",
		.owner = THIS_MODULE,
	}
};
module_platform_driver(xilinx_dphy_rx_driver);

MODULE_DESCRIPTION("xilinx MIPI DPHY Rx driver");
MODULE_LICENSE("GPL v2");
