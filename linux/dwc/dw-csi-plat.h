/* SPDX-License-Identifier: GPL-2.0 */
/*
 * Copyright (c) 2018 Synopsys, Inc.
 *
 * Synopsys DesignWare MIPI CSI-2 Host controller driver.
 * Supported bus formats
 *
 * Author: Luis Oliveira <Luis.Oliveira@synopsys.com>
 */

#ifndef _DW_CSI_PLAT_H__
#define _DW_CSI_PLAT_H__

#include "dw-mipi-csi.h"

struct mipi_fmt {
	u32 mbus_code;
	enum data_type ipi_dt;
};

/* Video formats supported by the MIPI CSI-2 */

static const struct mipi_fmt dw_mipi_csi_formats[] = {
	{
		.mbus_code = MEDIA_BUS_FMT_RGB666_1X18,
		.ipi_dt = CSI_2_RGB666,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB565_2X8_BE,
		.ipi_dt = CSI_2_RGB565,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB565_2X8_LE,
		.ipi_dt = CSI_2_RGB565,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB555_2X8_PADHI_BE,
		.ipi_dt = CSI_2_RGB555,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB555_2X8_PADHI_LE,
		.ipi_dt = CSI_2_RGB555,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB444_2X8_PADHI_BE,
		.ipi_dt = CSI_2_RGB444,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB444_2X8_PADHI_LE,
		.ipi_dt = CSI_2_RGB444,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB888_2X12_LE,
		.ipi_dt = CSI_2_RGB888,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_RGB888_2X12_BE,
		.ipi_dt = CSI_2_RGB888,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR8_1X8,
		.ipi_dt = CSI_2_RAW8,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGBRG8_1X8,
		.ipi_dt = CSI_2_RAW8,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGRBG8_1X8,
		.ipi_dt = CSI_2_RAW8,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SRGGB8_1X8,
		.ipi_dt = CSI_2_RAW8,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR10_2X8_PADHI_BE,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR10_2X8_PADHI_LE,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR10_2X8_PADLO_BE,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR10_2X8_PADLO_LE,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR10_1X10,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGBRG10_1X10,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGRBG10_1X10,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SRGGB10_1X10,
		.ipi_dt = CSI_2_RAW10,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR12_1X12,
		.ipi_dt = CSI_2_RAW12,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGBRG12_1X12,
		.ipi_dt = CSI_2_RAW12,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGRBG12_1X12,
		.ipi_dt = CSI_2_RAW12,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SRGGB12_1X12,
		.ipi_dt = CSI_2_RAW12,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR14_1X14,
		.ipi_dt = CSI_2_RAW14,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGBRG14_1X14,
		.ipi_dt = CSI_2_RAW14,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGRBG14_1X14,
		.ipi_dt = CSI_2_RAW14,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SRGGB14_1X14,
		.ipi_dt = CSI_2_RAW14,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SBGGR16_1X16,
		.ipi_dt = CSI_2_RAW16,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGBRG16_1X16,
		.ipi_dt = CSI_2_RAW16,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SGRBG16_1X16,
		.ipi_dt = CSI_2_RAW16,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_SRGGB16_1X16,
		.ipi_dt = CSI_2_RAW16,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_YVYU8_2X8,
		.ipi_dt = CSI_2_RAW8,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_VYUY8_1X16,
		.ipi_dt = CSI_2_YUV422_8,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_Y8_1X8,
		.ipi_dt = CSI_2_RAW8,
	},
	{
		.mbus_code = MEDIA_BUS_FMT_Y10_1X10,
		.ipi_dt = CSI_2_RAW8,
	},
};

#endif	/* _DW_CSI_PLAT_H__ */
