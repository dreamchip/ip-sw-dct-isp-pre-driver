/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef __ISP_PRE_V4L2_H__
#define __ISP_PRE_V4L2_H__

#include <media/v4l2-common.h>
#include <media/v4l2-device.h>
#include <isp-dma-v4l2.h>

typedef uintptr_t IspPreRegister_t;

#define MAX_SUBDEVS_CONNECTED	3
#define MAX_NUM_PADS			2

enum { ID_SUBDEV_PRE = 0, ID_SUBDEV_CSI2, ID_SUBDEV_UB954 };

struct isp_pre_buffer {
	struct vb2_v4l2_buffer vb;
	struct list_head list;
};

struct graph_entity {
	struct list_head list;
	struct device_node *node;
	struct media_entity *entity;

	struct v4l2_async_subdev asd;
	struct v4l2_subdev *subdev;
};

struct isp_pre_data {
	struct platform_device *pdev;
	struct clk *clk;
	struct reset_control *rst_noc;
	IspPreRegister_t __iomem regs;

	// v4l2 parts
	struct device *dev;
	struct v4l2_device v4l2_dev;
	struct v4l2_ctrl_handler ctrls;
	struct v4l2_subdev *sd[MAX_SUBDEVS_CONNECTED];
	struct v4l2_subdev pre_sd;
	struct v4l2_async_notifier notifier;
	struct media_device mdev;
	struct media_pad pads[MAX_NUM_PADS];
	struct list_head entities;
	struct isp_dma dma;
	struct media_pipeline pipe;

	// irq
	int irq_mipi_rx_idi2qpix;
	int irq_fifo_overlow;

	// stats
	struct {
		int irq_mipi_rx_idi2qpix_count;
		int irq_fifo_overlow_count;
		int frame_count;
	} stats;

};

int isp_pre_hw_init(struct platform_device *pdev);
int isp_pre_hw_stop(struct platform_device *pdev);
int isp_pre_probed(struct platform_device *pdev);

int isp_pre_create_sysfs(struct platform_device *pdev);
int isp_pre_remove_sysfs(struct platform_device *pdev);

#endif //__ISP_PRE_V4L2_H__
