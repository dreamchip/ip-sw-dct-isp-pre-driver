/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef __ISP_PRE_SYSCTRL_H__
#define __ISP_PRE_SYSCTRL_H__

#include <linux/types.h>
#include <linux/device.h>

#include "isp_pre_regs.h"

typedef uintptr_t IspPreRegister_t;

typedef enum {
	CHN_ID_MULTI_0 = 0, /* Multi Channel setup, ID of channel 0. */
	CHN_ID_MULTI_1 = 1, /* Multi Channel setup, ID of channel 1. */
	CHN_ID_MULTI_2 = 2, /* Multi Channel setup, ID of channel 2. */
	CHN_ID_MULTI_3 = 3, /* Multi Channel setup, ID of channel 3. */
	CHN_ID_SINGLE = 4, /* Single Channel setup always uses channel 0. */
} IspPreChannelId_t;

/**
 * @brief Filter which can be setup for each channel of the MIPI Sync stage.
 */
typedef enum {
	CHN_FILT_IDLE = 0, /* Idle, channel is disabled, data is discarded. */
	CHN_FILT_VC = 1, /* Match on virtual channel only.*/
	CHN_FILT_DT = 2, /* Match on data type only.*/
	CHN_FILT_VC_DT = 3, /* Match on both virtual channel and data type.*/
} IspPreChannelFilter_t;

typedef enum {
	ACQ_SYNC_ANY = 0, /* Change Acquisition on any short packet. */
	ACQ_SYNC_VC =
		1, /* Change Acquisition only on short packets of selected virtual channel. */
} IspPreAcqSyncMode_t;

/**
 * @brief Debug counters for selected channel.
 */
typedef struct {
	uint32_t frame_cnt; /* Frame counter. */
	uint32_t ln_cnt_current; /* Line counter within the currently received frame. */
	uint32_t ln_cnt_snapshot; /* Line counter of the last received frame. */
	uint32_t px_cnt_current; /* Pixel counter within the currently received frame. */
	uint32_t px_cnt_snapshot; /* Pixel counter of the last received frame. */
} IspPreDbgInfo_t;

/**
 * @brief Initialise the isp_pre pipeline.
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 *
 * @return          0 on success, a negative error code otherwise.
 */
int isp_pre_sysctrl_init(struct device *dev, IspPreRegister_t __iomem regs);

/**
 * @brief Cleanup the isp_pre pipeline.
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 *
 * @return          0 on success, a negative error code otherwise.
 */
int isp_pre_sysctrl_cleanup(struct device *dev, IspPreRegister_t __iomem regs);

/**
 * @brief Reset the isp_pre pipeline FIFO.
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 *
 * @return          0 on success, a negative error code otherwise.
 */
int isp_pre_sysctrl_flush(struct device *dev, IspPreRegister_t __iomem regs);

/**
 * @brief Configure one of the channels.
 *
 * There are two mutually exclusive options of using this function:
 * - Multi Channel Mode: Call it 4 times (once for each channel CHN_ID_MULTI_0
 *   to CHN_ID_MULTI_3). Make sure to disable channels that are unused by setting
 *   mode to CHN_FILT_IDLE.
 * - Single Channel Mode: Call function once with id set to CHN_ID_SINGLE.
 *   Data will be deinterleaved so that each component is written to a
 *   separate buffer.
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 * @param id        ID of the channel, see ChnId enum.
 * @param mode      Filter mode, to disable this channel use CHN_FILT_IDLE, for
 *                  details see ChnFilt enum.
 * @param vc        Virtual channel to filter for, if mode is set to CHN_FILT_VC
 *                  or CHN_FILT_VC_DT. Valid range is [0, 3]. Value is "don't
 *                  care" if mode is CHN_FILT_IDLE or CHN_FILT_DT.
 * @param dt        Data type to filter for, if mode is set to CHN_FILT_DT or
 *                  CHN_FILT_VC_DT. For valid values see MipiDt. The data type
 *                  must also be specified if filter mode is CHN_FILT_VC as it
 *                  is used to determine the pixel width. For mode CHN_FILT_IDLE
 *                  the data type is "don't care".
 * @return          0 on success, a negative error code otherwise.
 *
 */
int isp_pre_sysctrl_set_channel(struct device *dev,
				IspPreRegister_t __iomem regs,
				IspPreChannelId_t id,
				IspPreChannelFilter_t mode, int vc, int dt);

/**
 * @brief Configure acquisition start/stop behaviour.
 *
 * Changes how the ACQ module behaves when acquisition is started/stopped using
 * isp_pre_sysctrl_set_acq_enable().
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 * @param mode      Sync mode, see AcqSync.
 * @param sync_fs   If set acquisition of MIPI data starts after the next MIPI
 *                  frame start short packet as configured with mode and vc. If
 *                  deasserted acquisition starts immediately.
 * @param sync_fe   If set acquisition of MIPI data stops after the next MIPI
 *                  frame end short packet as configured with mode and vc. If
 *                  deasserted acquisition ends immediately.
 * @param vc        MIPI virtual channel that is used to filter for SOF/EOF
 *                  short packets. Valid range is [0, 3].
 *                  Only used if mode is set to ACQ_SYNC_VC.
 * @return          0 on success, negative error code otherwise:
 *                  -EINVAL: vc out of range.
 */
int isp_pre_sysctrl_set_acq_sync(struct device *dev,
				 IspPreRegister_t __iomem regs,
				 IspPreAcqSyncMode_t mode, bool sync_fs,
				 bool sync_fe, int vc);

/**
 * @brief Enable or disable MIPI data acquisition.
 *
 * Acqusition will start or stop depending on the configured synchronization
 * behaviour, see isp_pre_sysctrl_set_acq_sync(). This register is not shadowed!
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 * @param enable    Enable or disable data acquisition.
 */
void isp_pre_sysctrl_set_acq_enable(struct device *dev,
				    IspPreRegister_t __iomem regs,
				    bool enable);

/**
 * @brief Get current aquisition status
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 *
 * @return true if images are currently being captured (start condition has
 *         been met and data is being produced), false if data is discarded
 *         (acquisition has been stopped and stop condition was reached).
 */
bool isp_pre_sysctrl_get_acq_status(struct device *dev,
				    IspPreRegister_t __iomem regs);

/**
 * @brief Select which virtual channel is used for gathering debug data.
 *
 * @param dev   pointer to linux device
 * @param regs  pointer to I/O-mapped registers
 * @param vc    MIPI virtual channel which is used to select packets that
 *              are included in the debug counter calculation.
 * @return      0 on success, negative error code otherwise:
 *              -EINVAL: vc out of range.
 */
int isp_pre_sysctrl_set_debug_channel(struct device *dev,
				      IspPreRegister_t __iomem regs, int vc);

/**
 * @brief Get debug information.
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 *
 * @return  The gathered debug information, see DbgInfo.
 */
IspPreDbgInfo_t isp_pre_sysctrl_get_debug_info(struct device *dev,
					       IspPreRegister_t __iomem regs);

/**
 * @brief Print debug information to kernel log.
 *
 * @param dev       pointer to linux device
 * @param regs      pointer to I/O-mapped registers
 *
 * @return  The gathered debug information, see DbgInfo.
 */
void isp_pre_sysctrl_print_debug_info(struct device *dev,
				      IspPreRegister_t __iomem regs);

#endif // __ISP_PRE_SYSCTRL_H__
