/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
  */

#include <linux/types.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <media/mipi-csi2.h>

#include "isp_pre_v4l2.h"
#include "isp_pre_sysctrl.h"

#include "isp_pre_functions.h"
#include "dct_reg_helper.h"

int isp_pre_sysctrl_init(struct device *dev, IspPreRegister_t __iomem regs)
{
	uint32_t id = isp_pre_read(regs + ISP_PRE_ID_REG);

	/* Print module ID */
	dev_info(
		dev,
		"isp_pre_sysctrl: "
		"DCT ID: 0x%x, Project ID: 0x%x, Revision: %d, Module ID: 0x%x",
		DCT_REGS_FIELD_GET(id, DCT_ID),
		DCT_REGS_FIELD_GET(id, PROJECT_ID),
		DCT_REGS_FIELD_GET(id, REVISION),
		DCT_REGS_FIELD_GET(id, MODULE_ID));

	switch (DCT_REGS_FIELD_GET(id, REVISION)) {
	case 1:
		break;
	default:
		dev_err(dev,
			"isp-main-sysctrl: unsupported isp_main revision\n");
		return -ENODEV;
	}

	return 0;
}

int isp_pre_sysctrl_flush(struct device *dev, IspPreRegister_t __iomem regs)
{
	isp_pre_write(FIFO_FLUSH_MASK, regs + ISP_PRE_FIFO_FLUSH_REG);
	return 0;
}

int isp_pre_sysctrl_cleanup(struct device *dev, IspPreRegister_t __iomem regs)
{
	/* Disable image acquisition */
	isp_pre_sysctrl_set_acq_enable(dev, regs, false);
	msleep(40);
	isp_pre_sysctrl_flush(dev, regs);

	return 0;
}

static int isp_pre_sysctrl_set_channel_multi(struct device *dev,
					     IspPreRegister_t __iomem regs,
					     IspPreChannelId_t id,
					     IspPreChannelFilter_t mode, int vc,
					     int dt)
{
	uint32_t ch_filt;

	if (id == CHN_ID_SINGLE) {
		dev_err(dev,
			"IspPre: Can not use 'single channel ID' in multi channel mode");
		return -EINVAL;
	}
	if (vc < 0 || vc > 3) {
		/* In idle mode the virtual channel does not matter so set it to 0. */
		if (mode == CHN_FILT_IDLE) {
			vc = 0;
		} else {
			dev_err(dev,
				"IspPre: Virtual Channel %d out of valid range [0, 3].",
				vc);
			return -EINVAL;
		}
	}

	/* Get initial filt value */
	if (id < CHN_ID_MULTI_2) {
		ch_filt = isp_pre_read(regs + ISP_PRE_CH_FILT01_REG);
	} else {
		ch_filt = isp_pre_read(regs + ISP_PRE_CH_FILT23_REG);
	}

	/* Update values for this channel */
	switch (id) {
	case CHN_ID_MULTI_0:
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT0_MODE, mode);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT0_VC, vc);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT0_DT, dt);
		break;
	case CHN_ID_MULTI_1:
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT1_MODE, mode);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT1_VC, vc);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT1_DT, dt);
		break;
	case CHN_ID_MULTI_2:
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT2_MODE, mode);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT2_VC, vc);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT2_DT, dt);
		break;
	case CHN_ID_MULTI_3:
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT3_MODE, mode);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT3_VC, vc);
		DCT_REGS_FIELD_SET(ch_filt, CH_FILT3_DT, dt);
		break;
	default:
		/* Should never happen, range of chn is checked above */
		return -EINVAL;
	}

	/* Write back new value */
	if (id < CHN_ID_MULTI_2) {
		isp_pre_write(ch_filt, regs + ISP_PRE_CH_FILT01_REG);
	} else {
		isp_pre_write(ch_filt, regs + ISP_PRE_CH_FILT23_REG);
	}

	return 0;
}

static int isp_pre_sysctrl_set_channel_single(struct device *dev,
					      IspPreRegister_t __iomem regs,
					      IspPreChannelFilter_t mode,
					      int vc, int dt)
{
	uint32_t ch_filt = 0;

	if (vc < 0 || vc > 3) {
		/* In idle mode the virtual channel does not matter so set it to 0. */
		if (mode == CHN_FILT_IDLE) {
			vc = 0;
		} else {
			dev_err(dev,
				"IspPre: Virtual Channel %d out of valid range [0, 3].",
				vc);
			return -EINVAL;
		}
	}

	/* Reset filter settings for all channels */
	isp_pre_write(0, regs + ISP_PRE_CH_FILT01_REG);
	isp_pre_write(0, regs + ISP_PRE_CH_FILT23_REG);

	/* Setup filter for channel 0 */
	DCT_REGS_FIELD_SET(ch_filt, CH_FILT0_MODE, mode);
	DCT_REGS_FIELD_SET(ch_filt, CH_FILT0_VC, vc);
	DCT_REGS_FIELD_SET(ch_filt, CH_FILT0_DT, dt);
	isp_pre_write(ch_filt, regs + ISP_PRE_CH_FILT01_REG);

	return 0;
}

int isp_pre_sysctrl_set_channel(struct device *dev,
				IspPreRegister_t __iomem regs,
				IspPreChannelId_t id,
				IspPreChannelFilter_t mode, int vc, int dt)
{
	if (id == CHN_ID_SINGLE) {
		return isp_pre_sysctrl_set_channel_single(dev, regs, mode, vc,
							  dt);
	}

	return isp_pre_sysctrl_set_channel_multi(dev, regs, id, mode, vc, dt);
}

int isp_pre_sysctrl_set_acq_sync(struct device *dev,
				 IspPreRegister_t __iomem regs,
				 IspPreAcqSyncMode_t mode, bool sync_fs,
				 bool sync_fe, int vc)
{
	uint32_t acq_ctrl = isp_pre_read(regs + ISP_PRE_ACQ_CTRL_REG);

	if (vc < 0 || vc > 3) {
		dev_err(dev,
			"IspPre: Virtual Channel %d out of valid range [0, 3].",
			vc);
		return -EINVAL;
	}

	DCT_REGS_FLAG_ASSIGN(acq_ctrl, ACQ_SYNC_MODE, mode);
	DCT_REGS_FLAG_ASSIGN(acq_ctrl, ACQ_SYNC_FS, sync_fs);
	DCT_REGS_FLAG_ASSIGN(acq_ctrl, ACQ_SYNC_FE, sync_fe);
	DCT_REGS_FLAG_ASSIGN(acq_ctrl, ACQ_SYNC_VC, vc);
	isp_pre_write(acq_ctrl, regs + ISP_PRE_ACQ_CTRL_REG);

	return 0;
}

void isp_pre_sysctrl_set_acq_enable(struct device *dev,
				    IspPreRegister_t __iomem regs, bool enable)
{
	uint32_t acq_enable = isp_pre_read(regs + ISP_PRE_ACQ_ENABLE_REG);
	DCT_REGS_FLAG_ASSIGN(acq_enable, ACQ_ENABLE, enable);
	isp_pre_write(acq_enable, regs + ISP_PRE_ACQ_ENABLE_REG);
}

bool isp_pre_sysctrl_get_acq_status(struct device *dev,
				    IspPreRegister_t __iomem regs)
{
	uint32_t acq_status = isp_pre_read(regs + ISP_PRE_ACQ_STATUS_REG);
	return DCT_REGS_FLAG_GET(acq_status, ACQ_STATUS);
}

int isp_pre_sysctrl_set_debug_channel(struct device *dev,
				      IspPreRegister_t __iomem regs, int vc)
{
	uint32_t acq_ctrl = isp_pre_read(regs + ISP_PRE_ACQ_CTRL_REG);

	if (vc < 0 || vc > 3) {
		dev_err(dev,
			"IspPre: Virtual Channel %d out of valid range [0, 3].",
			vc);
		return -EINVAL;
	}

	DCT_REGS_FIELD_SET(acq_ctrl, DBG_CH_SEL, vc);
	isp_pre_write(acq_ctrl, ISP_PRE_ACQ_CTRL_REG);

	return 0;
}

IspPreDbgInfo_t isp_pre_sysctrl_get_debug_info(struct device *dev,
					       IspPreRegister_t __iomem regs)
{
	/* Read debug registers */
	uint32_t fc = isp_pre_read(regs + ISP_PRE_DBG_FC_REG);
	uint32_t lc = isp_pre_read(regs + ISP_PRE_DBG_LC_REG);
	uint32_t pc = isp_pre_read(regs + ISP_PRE_DBG_PC_REG);

	/* Extract slices */
	IspPreDbgInfo_t info = {
		DCT_REGS_FIELD_GET(fc, DBG_FRAME_CNT),
		DCT_REGS_FIELD_GET(lc, DBG_LN_CNT_CURRENT),
		DCT_REGS_FIELD_GET(lc, DBG_LN_CNT_SNAPSHOT),
		DCT_REGS_FIELD_GET(pc, DBG_PX_CNT_CURRENT),
		DCT_REGS_FIELD_GET(pc, DBG_PX_CNT_SNAPSHOT),
	};

	return info;
}

void isp_pre_sysctrl_print_debug_info(struct device *dev,
				      IspPreRegister_t __iomem regs)
{
	IspPreDbgInfo_t info = isp_pre_sysctrl_get_debug_info(dev, regs);

	dev_info(dev, "Debug Info\n");
	dev_info(dev, "  Acq Running:    %s\n",
		 isp_pre_sysctrl_get_acq_status(dev, regs) ? "yes" : "no");
	dev_info(dev, "  Frame Cnt:      %u\n", info.frame_cnt);
	dev_info(dev, "  Line Cnt:       %u\n", info.ln_cnt_current);
	dev_info(dev, "  Line Cnt Snap:  %u\n", info.ln_cnt_snapshot);
	dev_info(dev, "  Pixel Cnt:      %u\n", info.px_cnt_current);
	dev_info(dev, "  Pixel Cnt Snap: %u\n", info.px_cnt_snapshot);
}
