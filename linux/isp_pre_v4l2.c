/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

//#define DEBUG

#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/ioctl.h>
#include <linux/reset.h>
#include <linux/irqreturn.h>
#include <linux/kmod.h>
#include <linux/mfd/syscon.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/version.h>

#include <media/videobuf2-dma-contig.h>
#include <media/videobuf2-v4l2.h>

#include <media/v4l2-async.h>
#include <media/v4l2-common.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-device.h>
#include <media/v4l2-event.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-mediabus.h>
#include <media/videobuf2-dma-contig.h>
#include <media/videobuf2-v4l2.h>

#include <media/media-device.h>
#include <media/v4l2-mc.h>

#include "isp_pre_functions.h"
#include "dct_reg_helper.h"

#include "dwc/dw-mipi-csi.h"

#include "isp_pre_v4l2.h"
#include "isp_pre_regs.h"
#include "isp_pre_sysctrl.h"

#define MODULE_NAME "dct-isp-pre"
#define BEEP printk("dct-isp-pre:%s :%d\n", __func__, __LINE__);

/* -----------------------------------------------------------------------------
 * Helper function
 */
static struct isp_pre_data *isp_pre_get_priv(struct platform_device *pdev)
{
	if (pdev == NULL)
		return NULL;

	return platform_get_drvdata(pdev);
}

static inline struct isp_pre_buffer *
to_isp_pre_buffer(struct vb2_v4l2_buffer *vb2)
{
	return container_of(vb2, struct isp_pre_buffer, vb);
}

static struct isp_pre_data *notifier_to_isp_pre(struct v4l2_async_notifier *nf)
{
	return container_of(nf, struct isp_pre_data, notifier);
}

static void isp_pre_get_of_config(struct platform_device *pdev,
				  struct isp_pre_data *priv)
{
	struct device_node *parent_dn = pdev->dev.of_node;

	if (!parent_dn)
		return;
}

/* -----------------------------------------------------------------------------
 *
 */
static int isp_pre_priv_start_stream(struct device *dev)
{
	struct isp_pre_data *priv = dev_get_drvdata(dev);
	struct v4l2_subdev *subdev;
	int ret = 0;

	dev_dbg(dev, "%s\n", __func__);

	if (priv == NULL)
		return -1;

	dev_dbg(dev, "%s sub devices\n", "Starting");

	subdev = priv->sd[ID_SUBDEV_UB954];
	if (subdev == NULL)
		return -ENODEV;

	ret = v4l2_subdev_enable_streams(
		subdev, media_get_pad_index(&subdev->entity, 0, 0),
		BIT(0));

	if (ret < 0 && ret != -ENOIOCTLCMD) {
		dev_err(dev, "enable_stream failed on subdev %s\n",
			priv->sd[ID_SUBDEV_UB954]->name);
		return ret;
	}

	// todo: fixme: Currently it is not possible to enter the LP-11 state
	// of the ds954 which his needed to bringup the dphy.
	// The only possibility	is to start the camera streaming in before to
	// have the LP-11 state between the delivered packets. With this
	// workaround we are able to initialize the dphy as it can trigger on
	// the stop state of the data lanes.
	// Be aware that the noncontinous clock mode must be set in the dt as
	// otherwise the stop state of the clock lanes will never be reached and
	// the probe of the dphy will fail.
	/* power-on/off csi2 subdevice */
	subdev = priv->sd[ID_SUBDEV_CSI2];
	if (subdev == NULL)
		return -ENODEV;

	ret = v4l2_subdev_call(subdev, core, s_power, true);
	if (ret < 0 && ret != -ENOIOCTLCMD) {
		dev_err(dev, "s_power on failed on subdev %s\n",
			priv->sd[ID_SUBDEV_CSI2]->name);
		return ret;
	}

	/* Start or stop image acquisition. ACQ is configured to start
	 * synchronized with frame start and stop immediately (see
	 * isp_pre_sysctrl_set_acq_sync() in isp_pre_prope()). 
	 * Needs to be done after the pipeline is started to assure
	 * that we do not miss a pixel and that we sync on frame boundary. */
	isp_pre_sysctrl_set_acq_enable(dev, priv->regs, true);

	if (ret == -ENOIOCTLCMD)
		ret = 0;

	return ret;
}

static int isp_pre_priv_stop_stream(struct device *dev)
{
	struct isp_pre_data *priv = dev_get_drvdata(dev);
	struct v4l2_subdev *subdev;
	int ret = 0;

	dev_dbg(dev, "%s\n", __func__);

	if (priv == NULL)
		return -1;

	dev_dbg(dev, "%s sub devices\n", "Stopping");

	/* Start or stop image acquisition. ACQ is configured to start
	 * synchronized with frame start and stop immediately (see
	 * isp_pre_sysctrl_set_acq_sync() in isp_pre_prope()). */
	isp_pre_sysctrl_cleanup(dev, priv->regs);

	/* power-on/off csi2 subdevice */
	subdev = priv->sd[ID_SUBDEV_CSI2];

	ret = v4l2_subdev_call(subdev, core, s_power, false);
	if (ret < 0 && ret != -ENOIOCTLCMD) {
		dev_err(dev, "s_power off failed on subdev %s\n",
			subdev->name);
		return ret;
	}

	subdev = priv->sd[ID_SUBDEV_UB954];
	ret = v4l2_subdev_disable_streams(
		subdev, media_get_pad_index(&subdev->entity, 0, 0),
		BIT(0));

	if (ret < 0 && ret != -ENOIOCTLCMD) {
		dev_err(dev, "disable_stream failed on subdev %s\n",
			subdev->name);
		return ret;
	}

	if (ret == -ENOIOCTLCMD)
		ret = 0;

	return ret;
}

static int isp_pre_start_stream(struct device *dev, bool start)
{
	int ret = 0;

	if (start) {
		ret = isp_pre_priv_start_stream(dev);
	} else {
		ret = isp_pre_priv_stop_stream(dev);
	}

	return ret;
}

static void isp_pre_frame_ready(struct device *dev, struct vb2_v4l2_buffer *v4l2_buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct isp_pre_data *priv = platform_get_drvdata(pdev);
	(void) v4l2_buf;
	priv->stats.frame_count++;
}

/* -----------------------------------------------------------------------------
 *
 */
int isp_pre_hw_init(struct platform_device *pdev)
{
	struct isp_pre_data *priv = isp_pre_get_priv(pdev);
	if (priv == NULL)
		return -1;

	dev_dbg(&pdev->dev, "%s\n", __func__);

	return 0;
}
EXPORT_SYMBOL(isp_pre_hw_init);

int isp_pre_hw_stop(struct platform_device *pdev)
{
	struct isp_pre_data *priv = isp_pre_get_priv(pdev);
	if (priv == NULL)
		return -1;

	dev_dbg(&pdev->dev, "%s\n", __func__);

	return 0;
}
EXPORT_SYMBOL(isp_pre_hw_stop);

int isp_pre_probed(struct platform_device *pdev)
{
	struct isp_pre_data *priv = isp_pre_get_priv(pdev);
	if (priv == NULL) {
		dev_err(&pdev->dev, "isp-pre not probed yet\n");
		return 0;
	}

	return 1;
}
EXPORT_SYMBOL(isp_pre_probed);

static void isp_pre_notify(struct v4l2_subdev *sd, unsigned int notification,
			   void *arg)
{
	dev_dbg(sd->dev, "dct-isp_pre: isp_pre_notify\n");
	switch (notification) {
	default:
		pr_err("oops\n");
		break;
	}
}

static int isp_pre_init(struct platform_device *pdev, struct isp_pre_data *priv)
{
	priv->pdev = pdev;

	INIT_LIST_HEAD(&priv->entities);

	isp_pre_get_of_config(pdev, priv);

	return 0;
}

static int isp_pre_dma_init(struct isp_pre_data *priv)
{
	u32 supported[] = {
		V4L2_PIX_FMT_SRGGB12,
	};
	int ret;

	ret = isp_dma_init(priv->dev, &priv->dma, "write", &priv->v4l2_dev,
			   &priv->pipe, isp_pre_start_stream, isp_pre_frame_ready);
	if (ret < 0) {
		dev_err(priv->dev, "Failed to initialize dma\n");
		return ret;
	}

	/* Tell DMA which formats are supported */
	ret = isp_dma_set_supported_formats(&priv->dma, supported,
					    ARRAY_SIZE(supported));
	if (ret < 0) {
		dev_err(priv->dev, "error %d ISP DMA failed to check formats\n",
			ret);
		return ret;
	}

	/* Set DMA mask of this device to the same value as the DMAs DMA mask,
	 * this ensures that we can use the same address range as the DMA when
	 * allocating memory. */
	ret = dma_coerce_mask_and_coherent(priv->dev,
					   isp_dma_get_dma_mask(&priv->dma));
	if (ret < 0) {
		dev_err(priv->dev, "dma_coerce_mask_and_coherent failed: %d\n",
			ret);
		return ret;
	}

	return 0;
}

static const struct media_device_ops isp_pre_media_ops = {
	.link_notify = v4l2_pipeline_link_notify,
};

static void isp_pre_init_media_device(struct device *dev,
				      struct isp_pre_data *priv)
{
	priv->mdev.dev = dev;
	strscpy(priv->mdev.model, "isp-pre", sizeof(priv->mdev.model));
	strscpy(priv->mdev.bus_info, "platform:isp-pre",
		sizeof(priv->mdev.bus_info));
	media_device_init(&priv->mdev);
	priv->mdev.ops = &isp_pre_media_ops;
	priv->mdev.hw_revision = isp_pre_read(priv->regs + ISP_PRE_ID_REG);
}

static const struct of_device_id csi2_of_match[] = {
	{
		.compatible = "snps,dw-csi",
	},
	{},
};
static const struct of_device_id isp_dma_of_match[] = {
	{
		.compatible = "dct,dct-isp-dma",
	},
	{},
};

static int pre_camera_sensor_bound(struct v4l2_async_notifier *notifier,
				   struct v4l2_subdev *subdev,
				   struct v4l2_async_subdev *asd)
{
	int ret = 0;
	struct isp_pre_data *priv = notifier_to_isp_pre(notifier);
	priv->sd[ID_SUBDEV_UB954] = subdev;
	dev_dbg(priv->dev, "dct-isp-pre subdev bound\n");

	return ret;
}

static void pre_camera_sensor_unbind(struct v4l2_async_notifier *notifier,
				     struct v4l2_subdev *subdev,
				     struct v4l2_async_subdev *asd)
{
	struct isp_pre_data *priv = notifier_to_isp_pre(notifier);
	dev_dbg(priv->dev, "dct-isp-pre subdev unbound\n");
}

/**
 * Called when all subdevs are ready we registered for
 */
static int pre_camera_sensor_complete(struct v4l2_async_notifier *notifier)
{
	int ret = 0;
	struct v4l2_device *v4l2_dev = notifier->v4l2_dev;
	struct isp_pre_data *priv = notifier_to_isp_pre(notifier);
	struct media_entity *source;
	struct media_entity *sink;
	u16 source_pad;
	u16 sink_pad;

	ret = v4l2_device_register_subdev_nodes(v4l2_dev);

	if (ret < 0) {
		dev_err(priv->dev, "error %d failed register subdev nodes\n",
			ret);
		return ret;
	}

	// create media link between pre subdev and DMA
	source = &priv->sd[ID_SUBDEV_PRE]->entity;
	sink = &priv->dma.video_dev.entity;
	source_pad = media_get_pad_index(source, 0, 0);
	sink_pad = media_get_pad_index(sink, true, 0);

	ret = media_create_pad_link(source, source_pad, sink, sink_pad,
				    MEDIA_LNK_FL_ENABLED);
	if (ret < 0) {
		dev_err(priv->dev,
			"error %d failed to link pads pre <-> dma\n", ret);
		return ret;
	}

	// create media link between csi2 subdev and DMA
	source = &priv->sd[ID_SUBDEV_CSI2]->entity;
	sink = &priv->sd[ID_SUBDEV_PRE]->entity;
	source_pad = media_get_pad_index(source, 0, 0);
	sink_pad =  media_get_pad_index(sink, true, 0);

	ret = media_create_pad_link(source, source_pad, sink, sink_pad,
				    MEDIA_LNK_FL_ENABLED);
	if (ret < 0) {
		dev_err(priv->dev,
			"error %d failed to link pads csi2 <-> pre\n", ret);
		return ret;
	}

	// create media link betweeen ds90ub954 subdev and csi2 subdev
	source = &priv->sd[ID_SUBDEV_UB954]->entity;
	sink = &priv->sd[ID_SUBDEV_CSI2]->entity;
	source_pad = media_get_pad_index(source, 0, 0);
	sink_pad = media_get_pad_index(source, 1, 0);

	ret = media_create_pad_link(source, source_pad, sink, sink_pad,
				    MEDIA_LNK_FL_ENABLED);
	if (ret < 0) {
		dev_err(priv->dev,
			"error %d failed to link pads ub940 <-> csi2 \n", ret);
		return ret;
	}

	ret = media_device_register(&priv->mdev);
	if (ret) {
		dev_err(priv->dev, "error %d registering media device\n", ret);
		return ret;
	}

	dev_dbg(priv->dev, "dct-isp-pre notifier complete\n");
	return ret;
}

static const struct v4l2_async_notifier_operations pre_camera_sensor_ops = {
	.bound = pre_camera_sensor_bound,
	.unbind = pre_camera_sensor_unbind,
	.complete = pre_camera_sensor_complete,
};

/**
 * Register subdevs we rely on so we get informed when they are ready
 */
static int isp_pre_register_deserializer(struct isp_pre_data *priv)
{
	struct v4l2_async_subdev *asd;
	struct fwnode_handle *fwnode;
	struct fwnode_handle *ep;
	int ret = 0;

	v4l2_async_nf_init(&priv->notifier);

	ep = fwnode_graph_get_endpoint_by_id(dev_fwnode(priv->dev), 0, 0, 0);

	fwnode = fwnode_graph_get_remote_endpoint(ep);
	fwnode_handle_put(ep);
	dev_dbg(priv->dev, "Found '%pOF'\n", to_of_node(fwnode));

	asd = v4l2_async_nf_add_fwnode(&priv->notifier, fwnode,
				       struct v4l2_async_subdev);
	fwnode_handle_put(fwnode);

	priv->notifier.ops = &pre_camera_sensor_ops;
	priv->notifier.v4l2_dev = priv->dma.v4l2_dev;
	ret = v4l2_async_nf_register(priv->dma.v4l2_dev, &priv->notifier);

	if (ret)
		v4l2_async_nf_cleanup(&priv->notifier);

	dev_dbg(priv->dev, "Registered notifier for '%pOF'\n",
		to_of_node(fwnode));
	return ret;
}

/**
 * As the csi2 driver is a v4l2 subdev that does not register via the
 * async api we need to get it manually from the device tree node and
 * register it by ourself.
 */
static int isp_pre_register_csi2(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = pdev->dev.of_node;
	struct device_node *csi2;
	struct platform_device *pdev_csi2;
	struct dw_csi *csi2_priv;
	int ret = 0;
	struct isp_pre_data *priv = platform_get_drvdata(pdev);

	/* get access to csi2 driver via the device tree node as this
	 * is an oldschool driver that does not use the async subdev api. */
	csi2 = of_parse_phandle(np, "csi2", 0);
	if (!csi2) {
		pr_err("No csi2 node found\n");
		return -EINVAL;
	}

	dev_dbg(dev, "got node: %s %s\n", csi2->name, csi2->full_name);
	if (!of_match_node(csi2_of_match, csi2)) {
		of_node_put(csi2);
		return -EINVAL;
	}

	pdev_csi2 = of_find_device_by_node(csi2);
	of_node_put(csi2);
	if (!pdev_csi2)
		return -ENODEV;
	csi2_priv = platform_get_drvdata(pdev_csi2);
	if (!csi2_priv) {
		platform_device_put(pdev_csi2);
		return -EPROBE_DEFER;
	}

	dev_dbg(dev, "received csi2 drvdata, mapped at 0x%p\n",
		csi2_priv->base_address);
	dev_dbg(dev, "registering v4l2 subdev csi2\n");

	ret = v4l2_device_register_subdev(priv->dma.v4l2_dev, &csi2_priv->sd);
	if (ret)
		dev_err(dev, "error %d failed to register subdev csi2\n", ret);

	priv->sd[ID_SUBDEV_CSI2] = &csi2_priv->sd;

	return 0;
}

/* -----------------------------------------------------------------------------
 * Interrupt handling
 */
static irqreturn_t isp_pre_irq_mipi_rx_idi2qpix(int irq, void *data)
{
	struct isp_pre_data *priv = data;
	priv->stats.irq_mipi_rx_idi2qpix_count++;

	return IRQ_HANDLED;
}

static irqreturn_t isp_pre_irq_fifo_overflow(int irq, void *data)
{
	struct isp_pre_data *priv = data;
	priv->stats.irq_fifo_overlow_count++;

	return IRQ_HANDLED;
}

static int isp_pre_register_irqs(struct isp_pre_data *priv)
{
	int ret = 0;

	priv->irq_mipi_rx_idi2qpix = fwnode_irq_get_byname(
		dev_fwnode(priv->dev), "mipi-rx-idi2qpix");
	if (priv->irq_mipi_rx_idi2qpix < 0) {
		ret = dev_err_probe(priv->dev, priv->irq_mipi_rx_idi2qpix,
				    "Failed to get mipi-rx-idi2qpix IRQ\n");
		goto err_irqs;
	}

	dev_info(priv->dev, "Request mipi-rx-idi2qpix IRQ %d\n",
		 priv->irq_mipi_rx_idi2qpix);
	ret = request_irq(priv->irq_mipi_rx_idi2qpix,
			  isp_pre_irq_mipi_rx_idi2qpix, 0, MODULE_NAME, priv);

	if (ret) {
		dev_err(priv->dev,
			"Unable to request mipi-rx-idi2qpix IRQ %d\n",
			priv->irq_mipi_rx_idi2qpix);
		priv->irq_mipi_rx_idi2qpix = -1;
		goto err_irqs;
	}

	priv->irq_fifo_overlow =
		fwnode_irq_get_byname(dev_fwnode(priv->dev), "fifo-overflow");
	if (priv->irq_fifo_overlow < 0) {
		ret = dev_err_probe(priv->dev, priv->irq_fifo_overlow,
				    "Failed to get fifo-overlow IRQ\n");
		goto err_irqs;
	}

	dev_info(priv->dev, "Request fifo-overflow IRQ %d\n",
		 priv->irq_fifo_overlow);
	ret = request_irq(priv->irq_fifo_overlow, isp_pre_irq_fifo_overflow, 0,
			  MODULE_NAME, priv);
	if (ret) {
		dev_err(priv->dev, "Unable to request fifo-overflow IRQ %d\n",
			priv->irq_fifo_overlow);
		priv->irq_fifo_overlow = -1;
		goto err_irqs;
	}

	return 0;

err_irqs:
	return -1;
}


int isp_pre_sd_get_fmt(struct v4l2_subdev *sd, struct v4l2_subdev_state *state,
			struct v4l2_subdev_format *format)
{
	dev_info(sd->dev, "isp_pre_sd_get_fmt()\n");

	return 0;
}

int isp_pre_sd_set_fmt(struct v4l2_subdev *sd, struct v4l2_subdev_state *state,
			struct v4l2_subdev_format *format)
{
	struct isp_pre_data *priv = dev_get_drvdata(sd->dev);
	struct media_entity *curr;
	int ret = 0;

	dev_info(sd->dev, "isp_pre_sd_set_fmt(pad:%d, width:%d, height: %d, code: %d, which: %d)\n",
		 format->pad, format->format.width, format->format.height, format->format.code,format->which);

	// only the source pads accept config changes
	format->pad = 0;

	media_device_for_each_entity(curr, &priv->mdev) {
		struct v4l2_subdev *subdev = NULL;

		dev_dbg(sd->dev, "iterate media devices subdev: %s", curr->name);
		// @todo when we support more than just one format on the capture side we
		// should configure the sensor driver too. For now it is left out as the
		// imx390 driver deadlocks when we set the format from here.
		// see bug ZKIBSP-112
		if ((strstr(curr->name, "ds90ub960"))
		|| (strstr(curr->name, "ds90ub953"))
		|| (strstr(curr->name, "dw-csi"))) {
			if (is_media_entity_v4l2_subdev(curr)) {
				subdev = media_entity_to_v4l2_subdev(curr);
				if (subdev) {
					dev_dbg(sd->dev, "set_fmt for subdev: %s", subdev->name);
					ret = v4l2_subdev_call(subdev, pad, set_fmt, NULL, format);
					if (ret < 0)
						dev_warn(priv->dev, "Failed to set format: %d", ret);
				}
			} else {
				dev_warn(sd->dev, " %s is no v4l2 subdev, skip", curr->name);
			}
		}
	}

	return ret;
}

static struct v4l2_subdev_pad_ops isp_pre_sd_pad_ops = {
	.get_fmt = isp_pre_sd_get_fmt,
	.set_fmt = isp_pre_sd_set_fmt,
};

static struct v4l2_subdev_ops isp_pre_sd_ops = {
	.pad = &isp_pre_sd_pad_ops,
};

/* -----------------------------------------------------------------------------
 * Module initialization
 */
static int isp_pre_probe(struct platform_device *pdev)
{
	struct isp_pre_data *priv;
	struct device *dev = &pdev->dev;
	void __iomem *regs;
	int ret = 0;

	dev_info(dev, "probe isp_pre 0.0.1\n");

	priv = devm_kzalloc(dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	priv->dev = dev;
	platform_set_drvdata(pdev, priv);

	regs = devm_platform_ioremap_resource(pdev, 0);
	if (IS_ERR(regs))
		return PTR_ERR(regs);

	priv->regs = (uintptr_t)regs;
	priv->rst_noc = devm_reset_control_get_optional_shared(dev, NULL);
	if (IS_ERR(priv->rst_noc)) {
		dev_warn(dev, "warning getting reset control failed\n");
		// for zukimo soc reset is (currently) not needed so it is
		// ok to continue without having the reset
	}

	priv->clk = devm_clk_get_enabled(dev, "isp_pre_clk");
	if (IS_ERR(priv->clk)) {
		dev_err(dev, "failed to retrieve isp_pre_clk\n");
		return PTR_ERR(priv->clk);
	}

	isp_pre_init_media_device(dev, priv);

	/* Setup and register the V4L2 device */
	priv->v4l2_dev.mdev = &priv->mdev;
	priv->v4l2_dev.ctrl_handler = &priv->ctrls;
	ret = v4l2_ctrl_handler_init(&priv->ctrls, 0);
	if (ret < 0) {
		dev_err(dev, "failed to initialize V4L2 ctrl handler\n");
		goto error;
	}
	ret = v4l2_device_register(dev, &priv->v4l2_dev);
	if (ret < 0) {
		dev_err(dev, "Failed to register v4l2 device\n");
		goto error;
	}

	// can return with EPROBE_DEFER when driver is not ready yet
	ret = isp_pre_dma_init(priv);
	if (ret) {
		dev_err(dev, "isp_pre_dma_init failed: %d\n", ret);
		goto error;
	}

	priv->dma.v4l2_dev->notify = isp_pre_notify;

	priv->sd[ID_SUBDEV_PRE] = &priv->pre_sd;
	// register ourself as subdevice to get informed about
	// format changes and forward them to our connected subdevices
	v4l2_subdev_init(priv->sd[ID_SUBDEV_PRE], &isp_pre_sd_ops);
	snprintf(priv->sd[ID_SUBDEV_PRE]->name, sizeof(priv->sd[ID_SUBDEV_PRE]->name),
		 "isp-pre-subdev");
	priv->sd[ID_SUBDEV_PRE]->entity.function = MEDIA_ENT_F_IO_V4L;
	priv->sd[ID_SUBDEV_PRE]->dev = dev;

	ret = v4l2_device_register_subdev(&priv->v4l2_dev, priv->sd[ID_SUBDEV_PRE]);
	if (ret < 0) {
		dev_err(dev, "v4l2_device_register_subdev failed: %d\n", ret);
		goto error;
	}

	priv->pads[0].flags = MEDIA_PAD_FL_SINK;
	priv->pads[1].flags = MEDIA_PAD_FL_SOURCE;

	ret = media_entity_pads_init(&priv->sd[ID_SUBDEV_PRE]->entity, 2, priv->pads);
	if (ret < 0) {
		dev_err(dev, "media_entity_pads_init failed\n");
		goto error;
	}

	// directly register sync subdev
	// can return with EPROBE_DEFER when driver is not ready yet
	ret = isp_pre_register_csi2(pdev);
	if (ret) {
		if (ret != -EPROBE_DEFER)
			dev_err(dev, "isp_pre_register_csi2 failed: %d\n", ret);
		goto error;
	}

	/* register notifier to get informed when the subdev is available */
	ret = isp_pre_register_deserializer(priv);
	if (ret) {
		if (ret != -EPROBE_DEFER)
			dev_err(dev,
				"isp_pre_register_deserializer failed: %d\n",
				ret);
		goto error;
	}

	ret = isp_pre_init(pdev, priv);
	if (ret) {
		dev_err(dev, "isp_pre_init failed: %d\n", ret);
		goto error;
	}

	/* Configure defaults for the MIPI filter */
	ret = isp_pre_sysctrl_init(dev, priv->regs);
	if (ret) {
		dev_err(dev, "isp_pre_sysctrl_init failed: %d\n", ret);
		goto error;
	}
	ret = isp_pre_sysctrl_set_channel(dev, priv->regs, CHN_ID_SINGLE,
					  CHN_FILT_VC_DT, 0, CSI_2_RAW12);
	if (ret) {
		dev_err(dev, "isp_pre_sysctrl_set_channel failed: %d\n", ret);
		goto error;
	}
	ret = isp_pre_sysctrl_set_acq_sync(dev, priv->regs, ACQ_SYNC_VC, true,
					   true, 0);
	if (ret) {
		dev_err(dev, "isp_pre_sysctrl_set_acq_sync failed: %d\n", ret);
		goto error;
	}

	ret = isp_pre_register_irqs(priv);
	if (ret) {
		if (ret != -EPROBE_DEFER)
			dev_err(dev, "isp_pre_register_irqs failed: %d\n", ret);
		goto error;
	}

	/* Set default format */
	{
		struct v4l2_format format = {
			.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE,
			.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_SRGGB12,
			.fmt.pix_mp.num_planes = 1,
			.fmt.pix_mp.width = 1936,
			.fmt.pix_mp.height = 1100,
		};
		ret = isp_dma_override_format(&priv->dma, &format);
		if (ret < 0) {
			dev_err(priv->dev, "isp_dma_override_format() failed: %d\n", ret);
			return ret;
		}
	}

	if (priv->rst_noc)
		reset_control_deassert(priv->rst_noc);

	dev_info(dev, "create sysfs entries\n");
	ret = isp_pre_create_sysfs(pdev);
	if (ret) {
		dev_err(dev, "isp_pre_create_sysfs failed: %d\n", ret);
		goto error;
	}

	dev_info(dev, "probed isp_pre (hw: 0x%0X)\n",
		isp_pre_read(priv->regs + ISP_PRE_ID_REG));

	return 0;

error:
	if (ret == -EPROBE_DEFER)
		dev_info(
			dev,
			"Probing deferred, one ore more sub-devices are not ready yet\n");

	v4l2_device_unregister_subdev(&priv->pre_sd);
	v4l2_ctrl_handler_free(&priv->ctrls);
	v4l2_device_unregister(&priv->v4l2_dev);
	isp_dma_cleanup(&priv->dma);

	media_device_unregister(&priv->mdev);
	media_device_cleanup(&priv->mdev);

	if (priv->irq_mipi_rx_idi2qpix >= 0)
		free_irq(priv->irq_mipi_rx_idi2qpix, priv);
	if (priv->irq_fifo_overlow >= 0)
		free_irq(priv->irq_fifo_overlow, priv);

	return ret;
}

static int isp_pre_remove(struct platform_device *pdev)
{
	struct isp_pre_data *priv = platform_get_drvdata(pdev);

	if (priv == NULL)
		return 0;

	isp_pre_remove_sysfs(pdev);
	v4l2_device_unregister_subdev(&priv->pre_sd);
	v4l2_async_nf_unregister(&priv->notifier);
	v4l2_async_nf_cleanup(&priv->notifier);
	v4l2_ctrl_handler_free(&priv->ctrls);
	v4l2_device_unregister(&priv->v4l2_dev);
	isp_dma_cleanup(&priv->dma);

	media_device_unregister(&priv->mdev);
	media_device_cleanup(&priv->mdev);

	if (priv->irq_mipi_rx_idi2qpix >= 0)
		free_irq(priv->irq_mipi_rx_idi2qpix, priv);
	if (priv->irq_fifo_overlow >= 0)
		free_irq(priv->irq_fifo_overlow, priv);

	dev_info(&pdev->dev, "remove isp_pre\n");
	platform_set_drvdata(pdev, NULL);

	return 0;
}

static struct of_device_id isp_pre_of_match[] = {
	{
		.compatible = "dct,dct-isp-pre",
	},
	{},
};
MODULE_DEVICE_TABLE(of, isp_pre_of_match);

static struct platform_driver isp_pre_driver = {
    .probe = isp_pre_probe,
    .remove = isp_pre_remove,
    .driver =
        {
            .name = MODULE_NAME,
            .owner = THIS_MODULE,
            .of_match_table = of_match_ptr(isp_pre_of_match),
        },
};

static int __init isp_pre_driver_init(void)
{
	int ret;

	ret = platform_driver_register(&isp_pre_driver);
	if (ret)
		return ret;

	return ret;
}

static void __exit isp_pre_driver_exit(void)
{
	platform_driver_unregister(&isp_pre_driver);
}

module_init(isp_pre_driver_init);
module_exit(isp_pre_driver_exit);

MODULE_DESCRIPTION("DCT ISP-PRE driver");
MODULE_LICENSE("GPL v2");
