/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <linux/debugfs.h>

#include "isp_pre_v4l2.h"
#include "isp_pre_sysctrl.h"

static ssize_t debug_csiinfo_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct isp_pre_data *priv = platform_get_drvdata(pdev);

	char buffer[255];

	IspPreDbgInfo_t info = isp_pre_sysctrl_get_debug_info(dev, priv->regs);

	snprintf(buffer, 255,
		 "Acq Running:    %s\n"
		 "Frame Cnt:      %u\n"
		 "Line Cnt:       %u\n"
		 "Line Cnt Snap:  %u\n"
		 "Pixel Cnt:      %u\n"
		 "Pixel Cnt Snap: %u\n",
		 (isp_pre_sysctrl_get_acq_status(dev, priv->regs) ? "yes" :
								    "no"),
		 info.frame_cnt, info.ln_cnt_current, info.ln_cnt_snapshot,
		 info.px_cnt_current, info.px_cnt_snapshot);

	return strlcpy(buf, buffer, PAGE_SIZE);
}

static ssize_t debug_irqinfo_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct isp_pre_data *priv = platform_get_drvdata(pdev);

	char buffer[1024];
	int pos = 0;

	pos += snprintf(&buffer[pos], 1024,
			"MIPI_RX:        %d\n"
			"FIFO_OVERFLOW:  %d\n"
			"FRAME_COUNT:    %d\n",
			priv->stats.irq_mipi_rx_idi2qpix_count,
			priv->stats.irq_fifo_overlow_count,
			priv->stats.frame_count);

	return strlcpy(buf, buffer, PAGE_SIZE);
}


static ssize_t clear_irqinfo_show(struct device *dev,
				  struct device_attribute *attr, char *buf)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct isp_pre_data *priv = platform_get_drvdata(pdev);

	char buffer[1024];
	int pos = 0;

	priv->stats.irq_mipi_rx_idi2qpix_count = 0;
	priv->stats.irq_fifo_overlow_count = 0;
	priv->stats.frame_count = 0;

	pos += snprintf(&buffer[pos], 1024,
			"Interrupts cleared\n"
			"MIPI_RX:        %d\n"
			"FIFO_OVERFLOW:  %d\n"
			"FRAME_COUNT:    %d\n",
			priv->stats.irq_mipi_rx_idi2qpix_count,
			priv->stats.irq_fifo_overlow_count,
			priv->stats.frame_count);

	return strlcpy(buf, buffer, PAGE_SIZE);
}

static DEVICE_ATTR_RO(debug_csiinfo);
static DEVICE_ATTR_RO(debug_irqinfo);
static DEVICE_ATTR_RO(clear_irqinfo);

int isp_pre_create_sysfs(struct platform_device *pdev)
{
	device_create_file(&pdev->dev, &dev_attr_debug_csiinfo);
	device_create_file(&pdev->dev, &dev_attr_debug_irqinfo);
	device_create_file(&pdev->dev, &dev_attr_clear_irqinfo);

	return 0;
}

int isp_pre_remove_sysfs(struct platform_device *pdev)
{
	device_remove_file(&pdev->dev, &dev_attr_debug_csiinfo);
	device_remove_file(&pdev->dev, &dev_attr_debug_irqinfo);
	device_remove_file(&pdev->dev, &dev_attr_clear_irqinfo);

	return 0;
}
