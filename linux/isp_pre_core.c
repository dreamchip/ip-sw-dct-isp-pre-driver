/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */
#include <linux/types.h>
#include <linux/io.h>

uint32_t isp_pre_read(uintptr_t base)
{
	return ioread32((void __iomem *)base);
}

void isp_pre_write(uint32_t val, uintptr_t base)
{
	iowrite32(val, (void __iomem *)base);
}
