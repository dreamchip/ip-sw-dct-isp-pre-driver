/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 */
#ifndef __ISP_PRE_FUNCTIONS_H__
#define __ISP_PRE_FUNCTIONS_H__

uint32_t isp_pre_read(uintptr_t base);
void isp_pre_write(uint32_t val, uintptr_t base);

#endif